﻿GTKD
===================================
GTK+ wrapper for D language.

Status
-----------------------------------
The project is used by its author so it unlikely contains obvious bugs.
Project structure isn't ready yet so **API is subject to change**.
Also this is `dev` branch which revision history is temporary and may be changed.

So it's strongly recommended to contact the author if you want to use it seriously and you aren't a masochist.

Completeness
-----------------------------------
Due to lack of time only required for the application the project is used in functionality is implemented.

Features/goals
-----------------------------------
* Wrappers produces no GC garbage

	Marked as a feature as it is too common now for such wrappers to allocate GC memory on every `string` to *C-string* conversion.

* No blind auto-generation

	A human should check the result to avoid generator bugs.

License
-----------------------------------
The project is licensed under the terms of the [Boost Software License, Version 1.0](http://boost.org/LICENSE_1_0.txt).
