﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gdk.color;


import std.exception;

import unstd.c.string;

import gtkd.cutils;

import gtkc.gdktypes;
import gtkc.gdk;


@safe:

alias GDKException = Exception; // FIXME

struct Color
{
	private GdkColor _gdkColorData;
	static assert(this.sizeof == GdkColor.sizeof);

	this(ushort red, ushort green, ushort blue) pure nothrow
	{
		this.red = red;
		this.green = green;
		this.blue = blue;
	}

	this(in char[] spec) @trusted
	{
		enforceEx!GDKException(gdk_color_parse(spec.tempCString().assumeStringNotModified, gdkColor));
	}

	static Color fromRGB8(ubyte red, ubyte green, ubyte blue) pure nothrow
	{
		enum n = 0x101;
		return Color(red * n, green * n, blue * n);
	}

	static Color moveFromGDKColor(GdkColor* gdkColor)
	{
		const res = Color(gdkColor.red, gdkColor.green, gdkColor.blue);
		freeGDKColor(gdkColor);
		return res;
	}

	static GdkColor* copyGDKColor(in GdkColor* gdkColor) @trusted
	{ return gdk_color_copy(gdkColor.assumeNotModified); }

	static void freeGDKColor(GdkColor* gdkColor) @trusted
	{ gdk_color_free(gdkColor); }

	@property inout pure nothrow
	{
		inout(GdkColor)* gdkColor()
		{ return &_gdkColorData; }

		ref inout(ushort) red()
		{ return _gdkColorData.red; }

		ref inout(ushort) green()
		{ return _gdkColorData.green; }

		ref inout(ushort) blue()
		{ return _gdkColorData.blue; }
	}

	GdkColor* copyGDKColor() const
	{ return copyGDKColor(gdkColor); }

	hash_t opHash() const pure nothrow
	{
		// Same as gdk_color_hash
		return red + (green << 11) + (blue << 22) + (blue >> 6);
	}

	bool opEquals(const Color rhs) const pure nothrow
	{
		return opEquals(rhs);
	}

	bool opEquals(const ref Color rhs) const pure nothrow
	{
		return red == rhs.red && green == rhs.green && blue == rhs.blue;
	}

	string toString() const @trusted
	{
		return gdk_color_to_string(gdkColor.assumeNotModified).toStringAndGFree();
	}
}

@trusted unittest
{
	auto c = Color(1, 0xF2, 0xFFF3);
	assert(c.red == 1 && c.green == 0xF2 && c.blue == 0xFFF3);
	assert(c.opHash() == gdk_color_hash(c.gdkColor));
	++c.red;
	c.blue = 0;
	const cc = c;
	assert(cc == Color(1, 0xF3, 0));
	assert(Color.moveFromGDKColor(cc.copyGDKColor()) == cc);
	assert(Color(0, 0x101, 0xFFFF) == Color.fromRGB8(0, 1, 0xFF));
}
