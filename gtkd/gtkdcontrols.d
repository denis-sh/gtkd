﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtkdcontrols;


import std.string;

import metaui.markup;
import metaui.controls.native;
import metaui.controls.panelfactory;
import gtkd.gtk.utils;

import gtkc.gtktypes;

import gtk.Main;
import gtk.Widget;
import gtk.HBox;
import gtk.Button;
import gtk.Label;
import gtk.Entry;
import gtk.EntryBuffer;
import gtk.EditableIF;
import gtk.ToolButton;
import gtk.ToggleToolButton;
import gtk.MenuItem;

static import gtkd.gtk.controls;


@trusted: //FIXME?

HBox createHBox(Widget[] children...) {
	auto hbx = new HBox(false, 5);
	foreach(c; children)
		hbx.packStart(c, !cast(Label) c, true, 0);
	return hbx;
}

gtkd.gtk.controls.Widget toGTKDWidget(Widget widget)
{
	import gtkc.gtk;

	auto bx = gtkd.gtk.controls.Box.newHorizontal(0);
	bx.entity.gtk_box_pack_start(widget.getWidgetStruct(), true, true, 0);
	bx.show();
	return bx;
}

Widget toGtkDWidget(gtkd.gtk.controls.Widget widget)
{
	import gtkc.gtk;

	// FIXME return ctrlFile.rootControl.native.dynamicCast!Widget; GtkD <-> GTKD
	auto vbx = new HBox(false, 0);
	vbx.getBoxStruct().gtk_box_pack_start(widget.gtkWidget, true, true, 0);
	vbx.show();
	return vbx;
}

class NativeGTKDWidget: INativeControl
{
	Widget widget;

	this(Widget widget)
	{
		this.widget = widget;
		widget.addOnShow(&onVisibleChanged);
		widget.addOnHide(&onVisibleChanged);
	}

	private void onVisibleChanged(Widget)
	{
		if(!settingVisible && onVisibleChangedByUserCallback)
			onVisibleChangedByUserCallback(!!widget.getVisible());
	}


	private bool settingVisible = false;
	private void delegate(bool) @safe onVisibleChangedByUserCallback;

	// INativeControl implementation

	override void setVisible(bool visible)
	{
		settingVisible = true;
		widget.setVisible(visible);
		settingVisible = false;
	}

	override void setEnabled(bool enabled)
	{ widget.setSensitive(enabled); }


	override void setOnVisibleChangedByUserCallback(void delegate(bool) @safe callback)
	{ onVisibleChangedByUserCallback = callback; }
}

class NativeGTKDButton: NativeGTKDWidget, INativeActionControl
{
	Button button;

	this(Button button)
	{
		super(this.button = button);
		button.addOnClicked(&onClicked);
	}

	private void onClicked(Button)
	{
		if(onActivatedByUserCallback)
			onActivatedByUserCallback();
	}

	private void delegate() @safe onActivatedByUserCallback;


	// INativeActionControl implementation

	override void setOnActivatedByUserCallback(void delegate() @safe callback)
	{ onActivatedByUserCallback = callback; }
}

class NativeGTKDToolButton: NativeGTKDWidget, INativeActionControl
{
	ToolButton toolButton;

	this(ToolButton toolButton)
	{
		super(this.toolButton = toolButton);
		toolButton.addOnClicked(&onClicked);
	}

	private void onClicked(ToolButton)
	{
		if(onActivatedByUserCallback)
			onActivatedByUserCallback();
	}

	private void delegate() @safe onActivatedByUserCallback;


	// INativeActionControl implementation

	override void setOnActivatedByUserCallback(void delegate() @safe callback)
	{ onActivatedByUserCallback = callback; }
}

class NativeGTKDMenuItem: NativeGTKDWidget, INativeActionControl
{
	MenuItem menuItem;

	this(MenuItem menuItem)
	{
		super(this.menuItem = menuItem);
		menuItem.addOnActivate(&onActivate);
	}

	private void onActivate(MenuItem)
	{
		if(onActivatedByUserCallback)
			onActivatedByUserCallback();
	}

	private void delegate() @safe onActivatedByUserCallback;


	// INativeActionControl implementation

	override void setOnActivatedByUserCallback(void delegate() @safe callback)
	{ onActivatedByUserCallback = callback; }
}

class NativeGTKDToggleToolButton: NativeGTKDToolButton, INativeBoolInputControl
{
	ToggleToolButton toggleToolButton;
	bool setting = false;

	this(ToggleToolButton toggleToolButton)
	{
		super(this.toggleToolButton = toggleToolButton);
		toggleToolButton.addOnToggled(&onToggled);
	}

	private void onToggled(ToggleToolButton)
	{
		if(onValueChangedByUserCallback)
			onValueChangedByUserCallback(!!toggleToolButton.getActive());
	}

	private void delegate(bool) @safe onValueChangedByUserCallback;

	// INativeTextInputControl implementation

	override void setValue(bool val)
	{
		setting = true;
		toggleToolButton.setActive(val);
		setting = false;
	}

	override void setOnValueChangedByUserCallback(void delegate(bool) @safe callback)
	{ onValueChangedByUserCallback = callback; }
}

class NativeGTKDLabel: NativeGTKDWidget, INativeTextOutputControl
{
	Label label;

	string[string] tagsG;

	this(Label label)
	{
		tagsG = [null: "b"];
		super(this.label = label);
	}

	// INativeTextOutputControl implementation

	override void setTaggedText(string text, in Tag[] tags)
	{
		label.setMarkup(taggedTextToGMarkup(text, tagsG, tags));
	}
}

class NativeGTKDEntry: NativeGTKDWidget, INativeTextInputControl
{
	EntryBuffer entryBuffer;
	GtkEntryBuffer* entryBufferC;
	Entry entry;
	bool setting = false;

	this(Entry entry)
	{
		entryBuffer = entry.getBuffer();
		entryBufferC = entryBuffer.getEntryBufferStruct();
		super(this.entry = entry);
		entry.addOnChanged(&onTextChanged);
	}

	private void onTextChanged(EditableIF)
	{
		if(!setting && onTextChangedByUserCallback)
			onTextChangedByUserCallback(entryBufferC.internalBuffer.idup);
	}

	private void delegate(string) @safe onTextChangedByUserCallback;

	// INativeTextInputControl implementation

	override void setText(string text)
	{
		setting = true;
		entryBufferC.text = text ? text : "";
		setting = false;
	}

	override void setOnTextChangedByUserCallback(void delegate(string) @safe callback)
	{ onTextChangedByUserCallback = callback; }
}

final class SimpleGTKDPanelFactory(Base, NativePanels...): Base
{
	mixin nativePanelFactoryBase!NativePanels;
}
