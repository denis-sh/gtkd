﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtk.base;


import std.exception;
import std.string;
import std.traits;


class GTKConstructionException: Exception
{
	@safe pure nothrow this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    { super("Construction failure, " ~ msg, file, line, next); }

    @safe pure nothrow this(string msg, Throwable next, string file = __FILE__, size_t line = __LINE__)
    { this(msg, file, line, next); }
}

// FIXME template bloat?
auto gtkNewVA(alias func, string file = __FILE__, size_t line = __LINE__, A...)(A args)
{
	return enforceEx!GTKConstructionException(func(args), format("null returned by %s", __traits(identifier, func)), file, line);
}

// FIXME no var args support
auto gtkNew(alias func)(ParameterTypeTuple!func args, string file = __FILE__, size_t line = __LINE__)
{
	return enforceEx!GTKConstructionException(func(args), format("null returned by %s", __traits(identifier, func)), file, line);
}

@safe package struct UserEvent(A...)
{
	static if(A.length)
		bool setting = false;
	else
		enum setting = false;

	bool connected = false;

	alias UserCallback = void delegate(A);

	UserCallback callback;

	void tryCall(A a)
	{ if(!setting && callback) callback(a); }

	bool setCallback(UserCallback callback) pure
	{
		this.callback = callback;
		if(!callback || connected)
			return false;
		connected = true;
		return true;
	}
}

package template GTKSignal(alias connect, A...)
{
	alias GTKSignal = GTKSignal!(void, connect, A);
}

package struct GTKSignal(R, alias connect, A...)
{
	//  dmd @@@BUG@@@ workaround
	// alias Owner = __traits(parent, connect);
	alias Owner = typeof(__traits(parent, connect).init);
	alias Listener = R delegate(Owner, A);

	Owner owner;
	bool connected;
	Listener[] listeners;


	R callListeners(A args)
	{
		foreach(listener; listeners)
		{
			static if(is(R == void))
			{
				listener(owner, args);
			}
			else static if(is(R == bool))
			{
				if(listener(owner, args))
					return true;
			}
			else
				static assert(0);
		}
		static if(is(R == bool))
			return false;
	}

	void opOpAssign(string op : "~")(Listener listener)
	{
		if(!connected)
			__traits(getMember, owner, __traits(identifier, connect))();
		connected = true;
		listeners ~= listener;
	}
}

mixin template initGTKSignals()
{
	void initSignals()
	{
		foreach(name; __traits(derivedMembers, typeof(this)))
			static if(is(typeof(__traits(getMember, this, name)) U) && is(U: GTKSignal!(R, connect, A), R, alias connect, A...))
				__traits(getMember, this, name).owner = this;
	}
}
