﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtk.utils;


import gtkd.cutils;
public import gtkd.glib.utils;

import gtkc.gtktypes;
import gtkc.gtk;


@trusted:

@property const(char)[] internalBuffer(const GtkEntryBuffer* entryBuffer)
{
	immutable bytes = entryBuffer.assumeNotModified.gtk_entry_buffer_get_bytes();
	return entryBuffer.assumeNotModified.gtk_entry_buffer_get_text()[0 .. bytes];
}

@property void text(GtkEntryBuffer* entryBuffer, in char[] val)
{ entryBuffer.gtk_entry_buffer_set_text(val.ptr.assumeStringNotModified, val.chars); }
