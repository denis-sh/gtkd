﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtk.extcontrols;


import std.algorithm;
import std.array: empty, front;
import std.ascii;
import std.exception;
import std.format;
import std.functional;
//import std.math; // lround FIXME not implemented
static import std.string;

import unstd.c.string;

import unstdexp.utils;
import unstdexp.decimalstore;

import metaui.markup;
import metaui.controls.native;
import metaui.controls.panelfactory;

import gtkd.gobject.value;
import gtkd.cutils;
import gtkd.gtk.utils;
import gtkd.gtk.base;
import gtkd.gtk.treemodel;
import gtkd.gobject.object_;

import gtkc.gobject;
import gtkc.gtktypes;
import gtkc.gtk;

import gtkd.gtk.controls;


@trusted:


// FIXME: not general?
class PanelTable: Table
{
	private int row = 0;
	uint columns = 2;

	this(uint columns = -1, int spacing = 5)
	{
		this.columns = columns;
		super(0, 0, false);
		entity.gtk_table_set_row_spacings(spacing);
		entity.gtk_table_set_col_spacings(spacing);
		borderWidth = spacing;
	}

	private enum AttachOptions expandFill = AttachOptions.EXPAND | AttachOptions.FILL, no = cast(AttachOptions) 0;

	void addFLine(const(char)[] format, Widget[] widgets...) @safe
	{
		addFLine(null, format, widgets);
	}

	void addFLine(out Label label, const(char)[] format, Widget[] widgets...) @safe
	{
		Label[1] labels;
		addFLine(labels, format, widgets);
		label = labels[0];
	}

	void addFLine(Label[] labels, const(char)[] format, Widget[] widgets...) @trusted
	{
		alias enforce = enforceEx!FormatException;
		string text = null;
		size_t labelIdx = 0;
		uint pos = 0;
		void tryAddLabel()
		{
			if(!text)
				return;
			auto ll = new Label(text);
			if(labels)
			{
				enforce(labelIdx < labels.length, "Not enough labels.");
				labels[labelIdx++] = ll;
			}
			attach(ll, pos, pos + 1, row, row + 1, no, no);
			++pos;
			text = null;
		}
		size_t widgetIdx = 0;
		bool escaped = false;
		L: for(;;)
		{
			auto r = format[escaped .. $].find('\\', '*', '|'); // `find` is @system
			escaped = false;
			const(char)[] before = format[0 .. $ - r[0].length], after = r[0][!r[0].empty .. $];
			if(!before.empty)
				text ~= before;
			final switch(r[1])
			{
				case 0: // not found
					assert(after.empty);
					if(before.empty)
						break L;
					break;
				case 1: // '\\'
					enforce(!after.empty, "Start of escape sequence \\ at end of string");
					enforce(after[0].isASCII() && `\*|`.canFind(after[0]), std.string.format("Undefined escape sequence '\\%s'", after.front));
					escaped = true;
					break;
				case 2: // '*'
					tryAddLabel();
					enforce(widgetIdx < widgets.length, "Not enough widgets.");
					attach(widgets[widgetIdx++], pos, pos + 1, row, row + 1, expandFill, no);
					++pos;
					break;
				case 3: // '|'
					tryAddLabel();
					break;
			}
			format = after;
		}
		if(labels)
			enforce(labelIdx == labels.length, "Too many labels.");
		enforce(widgetIdx == widgets.length, "Too many widgets.");
		tryAddLabel();
		++row;
	}

	void addText(in char[] text)
	{
		assert(columns != -1);
		auto ll = new Label(text);
		attach(ll, 0, columns, row, row + 1, no, no);
		++row;
	}

	void addWidget(Widget widget)
	{
		assert(columns != -1);
		attach(widget, 0, columns, row, row + 1, expandFill, no);
		++row;
	}
}

ScrolledWindow inScrolled(Widget widget, bool withViewport = false)
{
	auto res = new ScrolledWindow(GtkPolicyType.AUTOMATIC);
	if(withViewport)
		res.addWithViewport(widget);
	else
		res.add(widget);
	return res;
}

Box packVerticalThin(int spacing, Widget[] widgets...)
{ return Box.newVertical(spacing).with_!`false`(widgets); }

Box packHorizontalThin(int spacing, Widget[] widgets...)
{ return Box.newHorizontal(spacing).with_!`false`(widgets); }

Box packVerticalThin(Widget[] widgets...)
{ return packVerticalThin(0, widgets); }

Box packHorizontalThin(Widget[] widgets...)
{ return packHorizontalThin(0, widgets); }

Box packHorizontalThinFormat(const(char)[] format, Widget[] widgets...) @safe
{ return packHorizontalFormat(false, format, widgets); }

Box packHorizontalFormat(const(char)[] format, Widget[] widgets...) @safe
{ return packHorizontalFormat(true, format, widgets); }

Box packHorizontalFormat(in bool expandAndFillWidgets, const(char)[] format, Widget[] widgets...) @trusted
{
	auto hbx = Box.newHorizontal(0);

	alias enforce = enforceEx!FormatException;
	string text = null;
	uint pos = 0;
	size_t widgetIdx = 0;
	bool escaped = false;
	L: for(;;)
	{
		auto r = format[escaped .. $].find('\\', '*'); // `find` is @system
		escaped = false;
		const(char)[] before = format[0 .. $ - r[0].length], after = r[0][!r[0].empty .. $];
		if(!before.empty)
			text ~= before;
		final switch(r[1])
		{
			case 0: // not found
				assert(after.empty);
				if(before.empty)
					break L;
				break;
			case 1: // '\\'
				enforce(!after.empty, "Start of escape sequence \\ at end of string");
				enforce(after[0].isASCII() && `\*`.canFind(after[0]), std.string.format("Undefined escape sequence '\\%s'", after.front));
				escaped = true;
				break;
			case 2: // '*'
				if(text)
				{
					hbx.packStart(new Label(text), false);
					text = null;
				}
				enforce(widgetIdx < widgets.length, "Not enough widgets.");
				hbx.packStart(widgets[widgetIdx++], expandAndFillWidgets);
				break;
		}
		format = after;
	}
	assert(widgetIdx <= widgets.length);
	enforce(widgetIdx == widgets.length, "Too many widgets.");
	if(text)
		hbx.packStart(new Label(text), false);
	return hbx;
}

class UnsettableSpinButton: SpinButton, INativeUnsettableNumberInputControl
{
	private
	{
		GtkSpinButtonUpdatePolicy _updatePolicy;
		double _lower, _upper, _value;
		bool _haveValue = true, _justUserSet;
	}

	this(Adjustment adjustment = null, double climbRate = 0, uint digits = 0)
	{ super(adjustment, climbRate, digits); initialize(); }

	this(double min, double max, double step)
	{ super(min, max, step); initialize(); }

	static UnsettableSpinButton newWithStep(double step, double pageStep = 0)
	{
		auto res = new UnsettableSpinButton();
		auto adj = res.entity.gtk_spin_button_get_adjustment();
		adj.gtk_adjustment_set_step_increment(step);
		adj.gtk_adjustment_set_page_increment(pageStep ? pageStep : step * 10);
		return res;
	}

	private void initialize()
	{
		extern(C) gint gtkCallBackInput(GtkSpinButton*, gdouble* newValue, UnsettableSpinButton sender)
		{
			with(sender)
			{
				if(gtkEntry.gtk_entry_get_buffer().gtk_entry_buffer_get_bytes() == 0)
				{
					if(_haveValue.tryUpdate(false))
					{
						_onNumberUnset();
						numberUnset.tryCall();
					}
					return -1; // GTK_INPUT_ERROR
				}
				if(_haveValue.tryUpdate(true))
				{
					_onNumberSet();
					_justUserSet = true;
				}
			}
			return false;
		}

		connectSignal("input", cast(GCallback) &gtkCallBackInput);

		extern(C) gint gtkCallBackOutput(GtkSpinButton*, UnsettableSpinButton sender)
		{
			with(sender)
			{
				if(haveValue && _justUserSet.tryUpdate(false) && valueAsFloating == _value)
					valueChanged.tryCall(valueAsFixed);
				return !haveValue;
			}
		}

		connectSignal("output", cast(GCallback) &gtkCallBackOutput);
	}

	@property bool haveValue() const
	{ return _haveValue; }


	private UserEvent!() numberUnset;

	// INativeNumberInputControl reimplementation

	override void setNumber(DecimalStore number)
	{
		const justSetNumber = _haveValue.tryUpdate(true);
		if(justSetNumber)
			_onNumberSet();
		super.setNumber(number);
		// FIXME may emit outupt, not update!
		//if(justSetNumber)
		//	update();
	}

	override void setNumberAndBounds(DecimalStore number, DecimalStore minimum, DecimalStore maximum)
	{
		const justSetNumber = _haveValue.tryUpdate(true);
		if(justSetNumber)
			_onNumberSet();
		super.setNumberAndBounds(number, minimum, maximum);
		// FIXME may emit outupt, not update!
		//if(justSetNumber)
		//	update();
	}

	// INativeUnsettableNumberInputControl implementation

	override void setPlaceholderText(string text)
	{ placeholderText = text; }

	override void unsetNumber()
	{
		_haveValue = false;
		gtkEntry.gtk_entry_set_text("".assumeStringNotModified);
		_onNumberUnset();
	}

	override void setOnNumberUnsetByUserCallback(void delegate() @safe callback)
	{ numberUnset.setCallback(callback); }

	private void _onNumberUnset()
	in { assert(!haveValue); }
	body
	{
		_updatePolicy = gtkSpinButton.gtk_spin_button_get_update_policy();
		gtkSpinButton.gtk_spin_button_set_update_policy(GtkSpinButtonUpdatePolicy.UPDATE_IF_VALID);
		gtkSpinButton.gtk_spin_button_get_range(&_lower, &_upper);
		_value = valueAsFloating;
		gtkSpinButton.gtk_spin_button_set_range(_value, _value);
		valueChanged.setting = true;
	}

	private void _onNumberSet()
	in { assert(haveValue); }
	body
	{
		gtkSpinButton.gtk_spin_button_set_update_policy(_updatePolicy);
		gtkSpinButton.gtk_spin_button_set_range(_lower, _upper);
		valueChanged.setting = false;
	}
}


class ArraySelectionComboBox(T): ComboBox, INativeArraySelectionControl!T
{
	private
	{
		ListStore _listStore;
		ValueG[] delegate(in T) _getValuesG;
	}

	this(ValueG[] delegate(in T) @safe getValuesG, in TypeG[] types...)
	in { assert(getValuesG && !types.empty); }
	body
	{
		_getValuesG = getValuesG;
		super(_listStore = new ListStore(types));
	}


	@property inout(ListStore) listStore() inout
	{ return _listStore; }


	private UserEvent!size_t changed;

	// INativeArraySelectionControl!T implementation

	override void setArray(T[] array, size_t selectedIdx)
	{
		changed.setting = true;
		_listStore.clear();
		foreach(i, const el; array)
			_listStore.appendAllValuesG(_getValuesG(el));
		active = selectedIdx;
		changed.setting = false;
	}

	override void setSelectedIndex(size_t idx)
	{
		changed.setting = true;
		active = idx;
		changed.setting = false;
	}

	override void setOnSelectionChangedByUserCallback(void delegate(size_t) @safe callback)
	{
		extern(C) void gtkCallBack(GtkComboBox*, ArraySelectionComboBox sender)
		{ with(sender) changed.tryCall(active); }

		if(changed.setCallback(callback))
			connectSignal("changed", cast(GCallback) &gtkCallBack);
	}
}


class ArraySelectionTreeView(T): TreeView, INativeArraySelectionControl!T
{
	private
	{
		ListStore _listStore;
		ValueG[] delegate(in T) _getValuesG;
	}

	this(ValueG[] delegate(in T) @safe getValuesG, in TypeG[] types...)
	in { assert(getValuesG && !types.empty); }
	body
	{
		_getValuesG = getValuesG;
		super(_listStore = new ListStore(types));
	}


	@property inout(ListStore) listStore() inout
	{ return _listStore; }


	private UserEvent!size_t selectIndexChanged;

	// INativeArraySelectionControl!T implementation

	override void setArray(T[] array, size_t selectedIdx)
	{
		selectIndexChanged.setting = true;
		_listStore.clear();
		foreach(i, const el; array)
			_listStore.appendAllValuesG(_getValuesG(el));
		selection.selectIndex(listStore, selectedIdx);
		selectIndexChanged.setting = false;
	}

	override void setSelectedIndex(size_t idx)
	{
		selectIndexChanged.setting = true;
		selection.selectIndex(listStore, idx);
		selectIndexChanged.setting = false;
	}

	override void setOnSelectionChangedByUserCallback(void delegate(size_t) @safe callback)
	{
		if(selectIndexChanged.setCallback(callback))
			// dmd @@@Issue 10509@@@ workaround
			//selection.changed ~= s => selectIndexChanged.tryCall(s.getSelectedIndex(listStore));
			selection.changed ~= &_onSelectionChanged;
	}

	private void _onSelectionChanged(TreeSelection s)
	{ selectIndexChanged.tryCall(s.getSelectedIndex(listStore)); }
}


class IterableCollectionTreeView(T): TreeView, INativeIterableCollectionControl!T
{
	private
	{
		ListStore _listStore;
		ValueG[] delegate(in T) _getValuesG;
		TreeModelRow _row = TreeModelRow.init;
		bool _haveIter = false;
	}

	this(ValueG[] delegate(in T) @safe getValuesG, in TypeG[] types...)
	in { assert(getValuesG && !types.empty); }
	body
	{
		_getValuesG = getValuesG;
		super(_listStore = new ListStore(types));
	}


	@property inout(ListStore) listStore() inout
	{ return _listStore; }


	private UserEvent!size_t elementActivated;

	// INativeIterableCollectionControl!T implementation

	override void clear()
	{
		listStore.clear();
		_haveIter = false;
	}

	override void moveTo(size_t idx)
	{
		_haveIter = idx != -1;
		if(_haveIter)
			_row = listStore.rows[idx];
	}

	override void moveNext()
	{
		assert(_haveIter);
		_haveIter = _row.tryMoveForward();
	}

	override void append(T value)
	{
		assert(!_haveIter);
		listStore.appendAllValuesG(_getValuesG(value));
	}

	override void insertBefore(T value)
	{
		assert(_haveIter);
		auto r = listStore.insertBefore(_row);
		listStore.setAllValuesG(r, _getValuesG(value));
	}

	override void update(T value)
	{
		assert(_haveIter);
		listStore.setAllValuesG(_row, _getValuesG(value));
	}

	override void remove()
	{
		assert(_haveIter);
		_haveIter = listStore.remove(_row);
	}

	override void removeToEnd()
	{
		assert(_haveIter);
		while(listStore.remove(_row)) { }
		_haveIter = false;
	}

	override void setOnElementActivatedByUserCallback(void delegate(size_t) @safe callback)
	{
		extern(C) void gtkCallBack(GtkTreeView*, GtkTreePath* gtkTreePath, GtkTreeViewColumn* column, IterableCollectionTreeView sender)
		{
			gint depth;
			gint* pindices = gtkTreePath.gtk_tree_path_get_indices_with_depth(&depth);
			assert(depth == 1);
			with(sender) elementActivated.tryCall(pindices[0].gIntAsSize);
		}

		if(elementActivated.setCallback(callback))
			connectSignal("row-activated", cast(GCallback) &gtkCallBack);
	}
}
