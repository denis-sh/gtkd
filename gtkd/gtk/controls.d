﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtk.controls;


import std.functional;
import std.math; // lround FIXME not implemented

import unstd.c.string;

import unstdexp.utils;
import unstdexp.decimalstore;

import metaui.markup;
import metaui.controls.native;
import metaui.controls.panelfactory;

import gtkd.gobject.value;
import gtkd.cutils;
import gtkd.gtk.utils;
import gtkd.gtk.base;
import gtkd.gtk.treemodel;
import gtkd.gobject.object_;

import gtkc.gobject;
import gtkc.gtktypes;
import gtkc.gtk;


@trusted:

version(none)
{
import gobject.Signals;
import gtkc.gtk;
import gtk.Window;
import gtk.Button;
import gtk.EntryBuffer;
import gtk.SpinButton;
import gtk.CheckButton;
import gobject.ObjectG;
import glib.ConstructionException;
}


alias StockID = gtkc.gtktypes.StockID;
alias GtkIconSize = gtkc.gtktypes.GtkIconSize;
alias StockDesc = gtkc.gtktypes.StockDesc;
alias GtkSelectionMode = gtkc.gtktypes.GtkSelectionMode;
alias GtkTextDirection = gtkc.gtktypes.GtkTextDirection;
alias GtkPackType = gtkc.gtktypes.GtkPackType;


/*
TODO: default values in ctors?
TODO: own ctors vs static newXXX functions?
*/

class Widget: ObjectG, INativeControl
{
	@property inout(GtkWidget)* gtkWidget() inout
	{ return cast(inout(GtkWidget)*) gObject; }

	alias entity = gtkWidget;

	this(GtkWidget* gtkWidget)
	{
		super(cast(GObject*) gtkWidget);
		mixin initGTKSignals;
		initSignals();
	}


	void show()
	{ entity.gtk_widget_show(); }

	void hide()
	{ entity.gtk_widget_hide(); }

	void showAll()
	{ entity.gtk_widget_show_all(); }

	void queueDraw()
	{ entity.gtk_widget_queue_draw(); }

	void grabFocus()
	{ entity.gtk_widget_grab_focus(); }


	@property bool canFocus() const
	{ return !!entity.assumeNotModified.gtk_widget_get_can_focus(); }

	@property void canFocus(bool val)
	{ entity.gtk_widget_set_can_focus(val); }


	@property bool visible() const
	{ return !!entity.assumeNotModified.gtk_widget_get_visible(); }

	@property void visible(bool val)
	{ entity.gtk_widget_set_visible(val); }


	@property GtkTextDirection direction() const
	{ return entity.assumeNotModified.gtk_widget_get_direction(); }

	@property void direction(GtkTextDirection val)
	{ entity.gtk_widget_set_direction(val); }


	@property bool enabled() const
	{ return !!entity.assumeNotModified.gtk_widget_get_sensitive(); }

	@property void enabled(bool val)
	{ entity.gtk_widget_set_sensitive(val); }


	GTKSignal!(bool, connectDeleteEvent, GdkEvent*) deleteEvent;

	final void connectDeleteEvent()
	{
		extern(C) bool gtkCallBack(GtkWidget*, GdkEvent* event, Widget sender)
		{ return sender.deleteEvent.callListeners(event); }

		connectSignal("delete-event", cast(GCallback) &gtkCallBack);
	}


	private UserEvent!bool visibleChanged;

	// INativeControl implementation

	override void setVisible(bool visible)
	{
		visibleChanged.setting = true;
		this.visible = visible;
		visibleChanged.setting = false;
	}

	override void setEnabled(bool enabled)
	{ this.enabled = enabled; }


	override void setOnVisibleChangedByUserCallback(void delegate(bool) @safe callback)
	{
		extern(C) void gtkCallBack(GtkEditable*, Entry sender)
		{ with(sender) visibleChanged.tryCall(visible); }

		if(visibleChanged.setCallback(callback))
		{
			connectSignal("show", cast(GCallback) &gtkCallBack);
			connectSignal("hide", cast(GCallback) &gtkCallBack);
		}
	}
}


class Container: Widget
{
	@property inout(GtkContainer)* gtkContainer() inout
	{ return cast(inout(GtkContainer)*) gObject; }

	alias entity = gtkContainer;

	this(GtkContainer* gtkContainer)
	{ super(cast(GtkWidget*) gtkContainer); }

	void add(Widget child)
	{ entity.gtk_container_add(child.entity); }

	void remove(Widget child)
	{ entity.gtk_container_remove(child.entity); }

	void removeAll()
	{
		import gtkc.glib;
		// FIXME: an unoptimal GtkD algorithm
		if(GList* gList = entity.gtk_container_get_children())
			foreach_reverse(i; 0 .. gList.g_list_length())
				entity.gtk_container_remove(cast(GtkWidget*) gList.g_list_nth_data(i.sizeAsGUInt));
	}


	@property int borderWidth() const
	{ return entity.assumeNotModified.gtk_container_get_border_width(); }

	@property void borderWidth(int val)
	{ entity.gtk_container_set_border_width(val); }


	void childSetPropertyG(Widget child, string propertyName, ValueG valueG) // FIXME note: not in and even not const
	{
		gtk_container_child_set_property(entity, child.entity, propertyName.tempCString().assumeStringNotModified, valueG.gValue.assumeNotModified);
	}


	void childSetProperty(T)(Widget child, string propertyName, T value)
	{
		childSetPropertyG(child, propertyName, ValueG(value));
	}
}


class Bin: Container
{
	@property inout(GtkBin)* gtkBin() inout
	{ return cast(inout(GtkBin)*) gObject; }

	alias entity = gtkBin;

	this(GtkBin* gtkBin)
	{ super(cast(GtkContainer*) gtkBin); }


	Bin with_(Widget child)
	{
		add(child);
		return this;
	}


	@property inout(Widget) child() inout
	{
		return cast(inout) ObjectG.fromGObject!Widget(entity.assumeNotModified.gtk_bin_get_child());
	}
}


class Table: Container
{
	@property inout(GtkTable)* gtkTable() inout
	{ return cast(inout(GtkTable)*) gObject; }

	alias entity = gtkTable;

	this(GtkTable* gtkTable)
	{ super(cast(GtkContainer*) gtkTable); }

	this(uint rows, uint columns, bool homogeneous = false)
	{ this(cast(GtkTable*) gtkNew!gtk_table_new(rows, columns, homogeneous)); }


	void attach(Widget child, uint leftAttach, uint rightAttach, uint topAttach, uint bottomAttach, GtkAttachOptions xoptions = GtkAttachOptions.EXPAND | GtkAttachOptions.FILL, GtkAttachOptions yoptions = GtkAttachOptions.EXPAND | GtkAttachOptions.FILL, uint xpadding = 0, uint ypadding = 0)
	{
		entity.gtk_table_attach(child.entity, leftAttach, rightAttach, topAttach, bottomAttach, xoptions, yoptions, xpadding, ypadding);
	}
}


class Image: Widget/*Misc*/
{
	@property inout(GtkImage)* gtkImage() inout
	{ return cast(inout(GtkImage)*) gObject; }

	alias entity = gtkImage;

	this(GtkImage* gtkImage)
	{ super(cast(GtkWidget*) gtkImage); }

	this(StockID stockID, GtkIconSize size)
	{ this(cast(GtkImage*) gtkNew!gtk_image_new_from_stock(StockDesc[stockID].tempCString().assumeStringNotModified, size)); }


	void setFromStock(StockID stockID, GtkIconSize size)
	{ entity.gtk_image_set_from_stock(StockDesc[stockID].tempCString().assumeStringNotModified, size); }
}


class Button: Bin, INativeActionControl
{
	@property inout(GtkButton)* gtkButton() inout
	{ return cast(inout(GtkButton)*) gObject; }

	alias entity = gtkButton;

	this(GtkButton* gtkButton)
	{ super(cast(GtkBin*) gtkButton); }

	this()
	{ this(cast(GtkButton*) gtkNew!gtk_button_new()); }

	this(in char[] label)
	{ this(cast(GtkButton*) gtkNew!gtk_button_new_with_label(label.tempCString().assumeStringNotModified)); }

	this(StockID stockID)
	{ this(cast(GtkButton*) gtkNew!gtk_button_new_from_stock(StockDesc[stockID].tempCString().assumeStringNotModified)); }


	static Button newWithoutLabel(StockID stockID, IconSize iconSize = IconSize.BUTTON)
	{
		auto res = new Button();
		res.add(new Image(stockID, iconSize));
		return res;
	}

	static Button newWithCustomLabel(in char[] customLabel, StockID stockID, IconSize iconSize = IconSize.BUTTON)
	{
		auto res = new Button(customLabel);
		res.image = new Image(stockID, iconSize);
		return res;
	}

	@property void image(Widget image)
	{ entity.gtk_button_set_image(image.entity); }


	/+private GTKSignal!IAction activate;

	override void addOnActivate(void delegate(IAction) del)
	{
		if(!activate.connected)
		{
			extern(C) void callBack(GtkWidget*, Button sender)
			{ sender.activate.callListeners(sender); }

			connectSignal("activate", cast(GCallback) &callBack);
			activate.connected = true;
		}
		activate ~= del;
	}+/

	private UserEvent!() clicked;

	// INativeActionControl implementation

	override void setOnActivatedByUserCallback(void delegate() @safe callback)
	{
		extern(C) void gtkCallBack(GtkWidget*, typeof(this) sender)
		{ with(sender) clicked.tryCall(); }

		if(clicked.setCallback(callback))
			connectSignal("clicked", cast(GCallback) &gtkCallBack);
	}
}


class Label: /*Misc*/ Widget, INativeTextOutputControl
{
	@property inout(GtkLabel)* gtkLabel() inout
	{ return cast(inout(GtkLabel)*) gObject; }

	alias entity = gtkLabel;

	string[string] tagsG;

	this(GtkLabel* gtkLabel)
	{
		tagsG = [null: "b"];
		super(cast(GtkWidget*) gtkLabel);
	}

	this(in char[] str, bool mnemonic = true)
	{
		if(mnemonic)
			this(cast(GtkLabel*) gtkNew!gtk_label_new_with_mnemonic(str.tempCString().assumeStringNotModified));
		else
			this(cast(GtkLabel*) gtkNew!gtk_label_new(str.tempCString().assumeStringNotModified));
	}

	this(bool mnemonic = true)
	{ this(null, mnemonic); }

	@property const(char[]) textBuff() const
	{ return entity.assumeNotModified.gtk_label_get_text().asArray; }

	@property string textStr() const
	{ return textBuff.buffToString(); }

	@property void text(in char[] val)
	{ entity.gtk_label_set_text(val.tempCString().assumeStringNotModified); }

	// INativeTextOutputControl implementation

	override void setTaggedText(string text, in Tag[] tags)
	{
		// FIXME: not costum tags support yet
		entity.gtk_label_set_markup(taggedTextToGMarkup(text, tagsG, tags).tempCString().assumeStringNotModified);
	}


	@property bool lineWrap() const
	{ return !!entity.assumeNotModified.gtk_label_get_line_wrap(); }

	@property void lineWrap(bool val)
	{ entity.gtk_label_set_line_wrap(val); }
}


class EntryBuffer: ObjectG
{
	@property inout(GtkEntryBuffer)* gtkEntryBuffer() inout
	{ return cast(inout(GtkEntryBuffer)*) gObject; }

	alias entity = gtkEntryBuffer;

	this(GtkEntryBuffer* gtkEntryBuffer)
	{ super(cast(GObject*) gtkEntryBuffer); }

	this(in char[] initialString = null)
	{ this(gtkNew!gtk_entry_buffer_new(initialString.ptr.assumeStringNotModified, initialString.chars)); }


	// This string points to internally allocated storage in the buffer and must not be modified or stored.
	@property const(char)[] internalBuffer() const
	{ return entity.internalBuffer; }

	@property void text(in char[] val)
	{ entity.text = val; }


	/+private GTKSignal!IStringInputValue valueChanged;

	override void addOnValueChanged(void delegate(IStringInputValue) del)
	{
		if(!valueChanged.connected)
		{
			extern(C) void callBackInsertedText(GtkEntryBuffer* bufferStruct, guint position, gchar* chars, guint nChars, EntryBuffer sender)
			{ sender.valueChanged.callListeners(sender); }

			extern(C) void callBackDeletedText(GtkEntryBuffer* bufferStruct, guint position, guint nChars, EntryBuffer sender)
			{ sender.valueChanged.callListeners(sender); }

			connectSignal("inserted-text", cast(GCallback) &callBackInsertedText);
			connectSignal("deleted-text", cast(GCallback) &callBackDeletedText);
			valueChanged.connected = true;
		}
		valueChanged ~= del;
	}+/
}


class Entry: Widget, INativeTextInputControl, INativeErrorOutput
{
	@property inout(GtkEntry)* gtkEntry() inout
	{ return cast(inout(GtkEntry)*) gObject; }

	alias entity = gtkEntry;

	this(GtkEntry* gtkEntry)
	{ super(cast(GtkWidget*) gtkEntry); }

	this()
	{ this(cast(GtkEntry*) gtkNew!gtk_entry_new()); }

	this(EntryBuffer entryBuffer)
	in { assert(entryBuffer); }
	body
	{ this(cast(GtkEntry*) gtkNew!gtk_entry_new_with_buffer(entryBuffer.entity)); }


	@property inout(EntryBuffer) buffer() inout
	{
		return cast(inout) ObjectG.fromGObject!EntryBuffer(entity.assumeNotModified.gtk_entry_get_buffer());
	}


	@property const(char[]) textBuff() const
	{
		// Don't use `gtk_entry_get_text` as GtkEntryBuffer also has bytes count information.
		return entity.assumeNotModified.gtk_entry_get_buffer().internalBuffer;
	}

	@property string textStr() const
	{ return textBuff.buffToString(); }

	@property void text(in char[] val)
	{
		// Don't use `gtk_entry_set_text` as it has no length argument.
		entity.gtk_entry_get_buffer().text = val;
	}


	@property const(char[]) placeholderTextBuff() const
	{ return entity.assumeNotModified.gtk_entry_get_placeholder_text().asArray; }

	@property string placeholderTextStr() const
	{ return placeholderTextBuff.buffToString(); }

	@property void placeholderText(in char[] val)
	{ entity.gtk_entry_set_placeholder_text(val.tempCString().assumeStringNotModified); }


	@property int maxLength() const
	{ return entity.assumeNotModified.gtk_entry_get_max_length(); }

	@property void maxLength(int val)
	{ entity.gtk_entry_set_max_length(val); }


	@property int widthChars() const
	{ return entity.assumeNotModified.gtk_entry_get_width_chars(); }

	@property void widthChars(int val)
	{ entity.gtk_entry_set_width_chars(val); }


	private UserEvent!string textChanged;

	// INativeTextInputControl implementation

	override void setText(string val)
	{
		textChanged.setting = true;
		text = val ? val : "";
		textChanged.setting = false;
	}

	override void setOnTextChangedByUserCallback(void delegate(string) @safe callback)
	{
		extern(C) void gtkCallBack(GtkEditable*, Entry sender)
		{ with(sender) textChanged.tryCall(textStr); }

		if(textChanged.setCallback(callback))
			connectSignal("changed", cast(GCallback) &gtkCallBack);
	}

	// INativeErrorOutput implementation

	auto errorIconPos = GtkEntryIconPosition.PRIMARY;
	StockID errorStockID = StockID.DIALOG_ERROR;

	override void setError(string error)
	{
		if(error)
		{
			entity.gtk_entry_set_icon_activatable(errorIconPos, false);
			entity.gtk_entry_set_icon_tooltip_text(errorIconPos, error.tempCString().assumeStringNotModified);
		}
		entity.gtk_entry_set_icon_from_stock(errorIconPos, error ? StockDesc[errorStockID].tempCString().assumeStringNotModified : null);
	}
}


class Adjustment: ObjectG
{
	@property inout(GtkAdjustment)* gtkAdjustment() inout
	{ return cast(inout(GtkAdjustment)*) gObject; }

	alias entity = gtkAdjustment;

	this(GtkAdjustment* gtkAdjustment)
	{ super(cast(GObject*) gtkAdjustment); }

	// Sets $(D value) to $(D lower) and $(D pageSize) to $(D zero).
	this(double lower, double upper, double stepIncrement, double pageIncrement)
	{ this(lower, lower, upper, stepIncrement, pageIncrement, 0); }

	this(double value, double lower, double upper, double stepIncrement, double pageIncrement, double pageSize = 0)
	{ this(gtkNew!gtk_adjustment_new(value, lower, upper, stepIncrement, pageIncrement, pageSize)); }


	@property double value() const
	{ return entity.assumeNotModified.gtk_adjustment_get_value(); }

	@property void value(double val)
	{ entity.gtk_adjustment_set_value(val); }
}


class SpinButton: Entry, INativeNumberInputControl
{
	@property inout(GtkSpinButton)* gtkSpinButton() inout
	{ return cast(inout(GtkSpinButton)*) gObject; }

	private
	{
		bool _autoIncrements = true;
		double _pageIncrementK = 10;
		bool _unsettedByOwner = false;
	}

	alias entity = gtkSpinButton;

	this(GtkSpinButton* gtkSpinButton)
	{
		super(cast(GtkEntry*) gtkSpinButton);

		extern(C) void gtkCallBack(GtkSpinButton*, GParamSpec* pspec, SpinButton sender)
		{ with(sender) if(autoIncrements) updateAutoIncrements(); }

		connectSignal("notify::digits", cast(GCallback) &gtkCallBack, ConnectFlags.AFTER);

		updateAutoIncrements();

		extern(C) gint gtkCallBackOutput(GtkSpinButton*, SpinButton sender)
		{ return sender._unsettedByOwner; }

		connectSignal("output", cast(GCallback) &gtkCallBackOutput);
	}

	this(Adjustment adjustment = null, double climbRate = 0, uint digits = 0)
	{ this(cast(GtkSpinButton*) gtkNew!gtk_spin_button_new(adjustment ? adjustment.entity : null, climbRate, digits)); }

	this(double min, double max, double step)
	{ this(cast(GtkSpinButton*) gtkNew!gtk_spin_button_new_with_range(min, max, step)); }


	static SpinButton newWithStep(double step, double pageStep = 0)
	{
		auto res = new SpinButton();
		auto adj = res.entity.gtk_spin_button_get_adjustment();
		adj.gtk_adjustment_set_step_increment(step);
		adj.gtk_adjustment_set_page_increment(pageStep ? pageStep : step * 10);
		return res;
	}


	@property inout(Adjustment) adjustment() inout
	{
		return cast(inout) ObjectG.fromGObject!Adjustment(entity.assumeNotModified.gtk_spin_button_get_adjustment());
	}


	@property bool autoIncrements() const
	{ return _autoIncrements; }

	@property void autoIncrements(bool val)
	{
		if(_autoIncrements.tryUpdate(val) && val)
			updateAutoIncrements();
	}

	private void updateAutoIncrements()
	in { assert(autoIncrements); }
	body
	{
		const gdouble step = 1e-1 ^^ digits;
		entity.gtk_spin_button_set_increments(step, step * pageIncrementK);
	}


	@property double pageIncrementK() const
	{ return _pageIncrementK; }

	@property void pageIncrementK(double val)
	{
		if(_pageIncrementK.tryUpdate(val) && autoIncrements)
		{
			gdouble step, page;
			entity.gtk_spin_button_get_increments(&step, &page);
			entity.gtk_spin_button_set_increments(step, step * val);
		}
	}


	@property uint digits() const
	{ return entity.assumeNotModified.gtk_spin_button_get_digits(); }

	@property void digits(uint val)
	{ entity.gtk_spin_button_set_digits(val); }


	@property long valueAsIntegral() const
	in { assert(!digits); }
	body
	{ return cast(long) round(valueAsFloating); }

	@property double valueAsFloating() const
	{ return entity.assumeNotModified.gtk_spin_button_get_value(); }

	@property DecimalStore valueAsFixed() const
	{ return DecimalStore.roundFrom(digits, valueAsFloating); }

	@property void value(double val)
	{ entity.gtk_spin_button_set_value(val); }


	void update()
	{ entity.gtk_spin_button_update(); }


	protected/*FIXME? private*/ UserEvent!DecimalStore valueChanged;

	// INativeNumberInputControl implementation

	override void setDigits(uint digits)
	{
		this.digits = digits;
	}

	override void unsetNumber(string placeholderText)
	{
		_unsettedByOwner = true;
		gtkEntry.gtk_entry_set_text("".assumeStringNotModified);
		super.placeholderText = placeholderText;
	}

	override void setNumber(DecimalStore number)
	{
		_unsettedByOwner = false;
		assert(number.fracDigits == digits); // TODO needed?
		valueChanged.setting = true;
		value = number.realValue;
		valueChanged.setting = false;
	}

	override void setNumberAndBounds(DecimalStore number, DecimalStore minimum, DecimalStore maximum)
	{
		_unsettedByOwner = false;
		assert(number.fracDigits == digits && minimum.fracDigits == digits && maximum.fracDigits == digits); // TODO needed?
		valueChanged.setting = true;
		entity.gtk_spin_button_set_range(minimum.realValue, maximum.realValue);
		value = number.realValue;
		valueChanged.setting = false;
	}

	override void setOnNumberChangedByUserCallback(void delegate(DecimalStore) @safe callback)
	{
		extern(C) void gtkCallBack(GtkSpinButton*, SpinButton sender)
		{ with(sender) valueChanged.tryCall(valueAsFixed); }

		if(valueChanged.setCallback(callback))
			connectSignal("value-changed", cast(GCallback) &gtkCallBack, ConnectFlags.AFTER);
	}
}


class ToggleButton: Button, INativeBoolInputControl
{
	@property inout(GtkToggleButton)* gtkToggleButton() inout
	{ return cast(inout(GtkToggleButton)*) gObject; }

	alias entity = gtkToggleButton;

	this(GtkToggleButton* gtkToggleButton)
	{ super(cast(GtkButton*) gtkToggleButton); }

	this()
	{ this(cast(GtkToggleButton*) gtkNew!gtk_toggle_button_new()); }

	this(in char[] label, bool mnemonic = true)
	{
		if(mnemonic)
			this(cast(GtkToggleButton*) gtkNew!gtk_toggle_button_new_with_mnemonic(label.tempCString().assumeStringNotModified));
		else
			this(cast(GtkToggleButton*) gtkNew!gtk_toggle_button_new_with_label(label.tempCString().assumeStringNotModified));
	}

	static ToggleButton newWithCustomLabel(in char[] customLabel, StockID stockID, IconSize iconSize = IconSize.BUTTON)
	{
		auto res = new ToggleButton(customLabel);
		res.image = new Image(stockID, iconSize);
		return res;
	}


	@property bool active() const
	{ return !!entity.assumeNotModified.gtk_toggle_button_get_active(); }

	@property void active(bool val)
	{ entity.gtk_toggle_button_set_active(val); }


	@property bool inconsistent() const
	{ return !!entity.assumeNotModified.gtk_toggle_button_get_inconsistent(); }

	@property void inconsistent(bool val)
	{ entity.gtk_toggle_button_set_inconsistent(val); }


	private UserEvent!bool toggled;

	// INativeBoolInputControl implementation

	override void setValue(bool val)
	{
		toggled.setting = true;
		active = val;
		toggled.setting = false;
	}

	override void setOnValueChangedByUserCallback(void delegate(bool) @safe callback)
	{
		extern(C) void gtkCallBack(GtkToggleButton*, ToggleButton sender)
		{ with(sender) toggled.tryCall(active); }

		if(toggled.setCallback(callback))
			connectSignal("toggled", cast(GCallback) &gtkCallBack);
	}
}


class CheckButton: ToggleButton
{
	@property inout(GtkCheckButton)* gtkCheckButton() inout
	{ return cast(inout(GtkCheckButton)*) gObject; }

	alias entity = gtkCheckButton;

	this(GtkCheckButton* gtkCheckButton)
	{ super(cast(GtkToggleButton*) gtkCheckButton); }

	this()
	{ this(cast(GtkCheckButton*) gtkNew!gtk_check_button_new()); }

	this(in char[] label, bool mnemonic = true)
	{
		if(mnemonic)
			this(cast(GtkCheckButton*) gtkNew!gtk_check_button_new_with_mnemonic(label.tempCString().assumeStringNotModified));
		else
			this(cast(GtkCheckButton*) gtkNew!gtk_check_button_new_with_label(label.tempCString().assumeStringNotModified));
	}
}


class Box: Container
{
	@property inout(GtkBox)* gtkBox() inout
	{ return cast(inout(GtkBox)*) gObject; }

	alias entity = gtkBox;

	this(GtkBox* gtkBox)
	{ super(cast(GtkContainer*) gtkBox); }

	this(GtkOrientation orientation, int spacing)
	{ this(cast(GtkBox*) gtkNew!gtk_box_new(orientation, spacing)); }

	static Box newVertical(int spacing)
	{ return new Box(GtkOrientation.VERTICAL, spacing); }

	static Box newHorizontal(int spacing)
	{ return new Box(GtkOrientation.HORIZONTAL, spacing); }


	void packStart(Widget child, bool expand, bool fill, uint padding = 0)
	{
		entity.gtk_box_pack_start(child.entity, expand, fill, padding);
	}

	void packStart(Widget child, bool expandAndFill, uint padding = 0)
	{
		packStart(child, expandAndFill, expandAndFill, padding);
	}


	void packEnd(Widget child, bool expand, bool fill, uint padding = 0)
	{
		entity.gtk_box_pack_end(child.entity, expand, fill, padding);
	}

	void packEnd(Widget child, bool expandAndFill, uint padding = 0)
	{
		packEnd(child, expandAndFill, expandAndFill, padding);
	}


	void queryChildPacking(Widget child, out bool expand, out bool fill, out uint padding, out GtkPackType packType)
	{
		gint _expand, _fill;
		entity.gtk_box_query_child_packing(child.entity, &_expand, &_fill, &padding, &packType);
		expand = !!_expand;
		fill = !!_fill;
	}

	void setChildPacking(Widget child, bool expand, bool fill, uint padding = 0, GtkPackType packType = GtkPackType.START)
	{
		entity.gtk_box_set_child_packing(child.entity, expand, fill, padding, packType);
	}

	void setChildPacking(Widget child, bool expandAndFill, uint padding = 0, GtkPackType packType = GtkPackType.START)
	{
		setChildPacking(child, expandAndFill, expandAndFill, padding,  packType);
	}
}


Box with_(alias expandAndFillChild)(Box box, Widget[] children...) if(is(typeof(unaryFun!expandAndFillChild(children[0])) == bool))
{
	foreach(child; children)
		box.packStart(child, unaryFun!expandAndFillChild(child));
	return box;
}


class ScrolledWindow: Bin
{
	@property inout(GtkScrolledWindow)* gtkScrolledWindow() inout
	{ return cast(inout(GtkScrolledWindow)*) gObject; }

	alias entity = gtkScrolledWindow;

	this(GtkScrolledWindow* gtkScrolledWindow)
	{ super(cast(GtkBin*) gtkScrolledWindow); }

	this()
	{ this(cast(GtkScrolledWindow*) gtkNew!gtk_scrolled_window_new(null, null)); }

	this(GtkPolicyType hscrollbarPolicy, GtkPolicyType vscrollbarPolicy)
	{
		this();
		entity.gtk_scrolled_window_set_policy(hscrollbarPolicy, vscrollbarPolicy);
	}

	this(GtkPolicyType scrollbarsPolicy)
	{ this(scrollbarsPolicy, scrollbarsPolicy); }

	void addWithViewport(Widget child)
	{
		entity.gtk_scrolled_window_add_with_viewport(child.entity);
	}
}


class Window: Bin
{
	@property inout(GtkWindow)* gtkWindow() inout
	{ return cast(inout(GtkWindow)*) gObject; }

	alias entity = gtkWindow;

	this(GtkWindow* gtkWindow)
	{ super(cast(GtkBin*) gtkWindow); }

	this(GtkWindowType type = GtkWindowType.TOPLEVEL)
	{ this(cast(GtkWindow*) gtkNew!gtk_window_new(type)); }

	this(in char[] title, GtkWindowType type = GtkWindowType.TOPLEVEL)
	{
		this(type);
		this.title = title;
	}


	@property const(char[]) titleBuff() const
	{ return (cast(const) entity.assumeNotModified.gtk_window_get_title()).asArray; }

	@property string titleStr() const
	{ return titleBuff.buffToString(); }

	@property void title(in char[] val)
	{ entity.gtk_window_set_title(val.tempCString().assumeStringNotModified); }


	@property void destroyWithParent(bool val)
	{ entity.gtk_window_set_destroy_with_parent(val); }

	@property void skipTaskbarHint(bool val)
	{ entity.gtk_window_set_skip_taskbar_hint(val); }

	@property void typeHint(GdkWindowTypeHint val)
	{ entity.gtk_window_set_type_hint(val); }

	@property void transientParent(Window val)
	{ entity.gtk_window_set_transient_for(val.entity); }


	void resize(int width, int height)
	{ entity.gtk_window_resize(width, height); }
	
	
	void setIconFromFile(in char[] filename)
	{
		entity.gtk_window_set_icon_from_file(filename.tempCString().assumeStringNotModified, null);
	}

	void setIconName(in char[] name)
	{
		entity.gtk_window_set_icon_name(name.tempCString().assumeStringNotModified);
	}
}


class Alignment: Bin
{
	@property inout(GtkAlignment)* gtkAlignment() inout
	{ return cast(inout(GtkAlignment)*) gObject; }

	alias entity = gtkAlignment;

	this(GtkAlignment* gtkAlignment)
	{ super(cast(GtkBin*) gtkAlignment); }

	this(float xalign, float yalign, float xscale, float yscale)
	{ this(cast(GtkAlignment*) gtkNew!gtk_alignment_new(xalign, yalign, xscale, yscale)); }

	static Alignment newCenter()
	{ return new Alignment(0.5, 0.5, 0, 0); }

	static Alignment newEast()
	{ return new Alignment(1.0, 0.5, 0, 0); }

	static Alignment newWest()
	{ return new Alignment(0.0, 0.5, 0, 0); }
}


class Frame: Bin
{
	@property inout(GtkFrame)* gtkFrame() inout
	{ return cast(inout(GtkFrame)*) gObject; }

	alias entity = gtkFrame;

	this(GtkFrame* gtkFrame)
	{ super(cast(GtkBin*) gtkFrame); }

	this(in char[] label = null)
	{
		// FIXME ? downcast to correct type in fromGObject?
		// this(cast(GtkFrame*) gtkNew!gtk_frame_new(label.tempCString().assumeStringNotModified));
		this(cast(GtkFrame*) gtkNew!gtk_frame_new(null));
		if(label)
			this.label = label;
	}


	@property void label(in char[] label)
	{
		if(label)
		{
			auto ll = new Label(label); // FIXME
			ll.show();
			labelWidget = ll;
		}
		else
		{
			entity.gtk_frame_set_label(null);
		}
	}


	@property inout(Widget) labelWidget() inout
	{
		return cast(inout) ObjectG.fromGObject!Widget(entity.assumeNotModified.gtk_frame_get_label_widget());
	}

	@property void labelWidget(Widget widget)
	{
		entity.gtk_frame_set_label_widget(widget.entity);
	}
}


class Paned: Container
{
	@property inout(GtkPaned)* gtkPaned() inout
	{ return cast(inout(GtkPaned)*) gObject; }

	alias entity = gtkPaned;

	this(GtkPaned* gtkPaned)
	{ super(cast(GtkContainer*) gtkPaned); }

	this(GtkOrientation orientation)
	{ this(cast(GtkPaned*) gtkNew!gtk_paned_new(orientation)); }

	this(GtkOrientation orientation, int position)
	{
		this(orientation);
		this.position = position;
	}

	static Paned newVertical(int position)
	{ return new Paned(GtkOrientation.VERTICAL, position); }

	static Paned newHorizontal(int position)
	{ return new Paned(GtkOrientation.HORIZONTAL, position); }


	Paned withPair(Widget child1, Widget child2)
	{
		addPair(child1, child2);
		return this;
	}

	void addPair(Widget child1, Widget child2)
	{ add1(child1); add2(child2); }

	void add1(Widget child)
	{ pack1(child); }

	void add2(Widget child)
	{ pack2(child); }

	void pack1(Widget child, bool resize = true, bool shrink = true)
	{ entity.gtk_paned_pack1(child.entity, resize, shrink); }

	void pack2(Widget child, bool resize = true, bool shrink = true)
	{ entity.gtk_paned_pack2(child.entity, resize, shrink); }


	@property int position() const
	{ return entity.assumeNotModified.gtk_paned_get_position(); }

	@property void position(int val)
	{ entity.gtk_paned_set_position(val); }
}


class TreeSelection: Container
{
	@property inout(GtkTreeSelection)* gtkTreeSelection() inout
	{ return cast(inout(GtkTreeSelection)*) gObject; }

	alias entity = gtkTreeSelection;

	this(GtkTreeSelection* gtkTreeSelection)
	{
		super(cast(GtkContainer*) gtkTreeSelection);
		mixin initGTKSignals;
		initSignals();
	}

	void selectAll()
	{ entity.gtk_tree_selection_select_all(); }

	void unselectAll()
	{ entity.gtk_tree_selection_unselect_all(); }


	@property void mode(GtkSelectionMode val)
	{ entity.gtk_tree_selection_set_mode(val); }


	GTKSignal!connectChanged changed;

	final void connectChanged()
	{
		extern(C) void gtkCallBack(GtkWidget*, TreeSelection sender)
		{ sender.changed.callListeners(); }

		connectSignal("changed", cast(GCallback) &gtkCallBack);
	}

	// FIXME not here?
	void selectIndex(ITreeModel model, size_t index)
	{
		if(index == -1)
		{
			unselectAll();
			return;
		}
		auto gtkTreePath = model.entity.gtk_tree_model_get_path(model.rows[index].iter);
		scope(exit) gtkTreePath.gtk_tree_path_free();
		entity.gtk_tree_selection_select_path(gtkTreePath);
	}

	// FIXME not here?
	void selectRow(TreeModelRow row)
	{
		entity.gtk_tree_selection_select_iter(row.iter);
	}

	// FIXME not here?
	size_t getSelectedIndex(in ITreeModel model) const
	{
		GtkTreeIter iter;
		if(!entity.assumeNotModified.gtk_tree_selection_get_selected(null, &iter))
			return -1;

		auto gtkTreePath = model.entity.assumeNotModified.gtk_tree_model_get_path(&iter);
		scope(exit) gtkTreePath.gtk_tree_path_free();
		int depth;
		auto pindices = gtkTreePath.gtk_tree_path_get_indices_with_depth(&depth);
		assert(depth == 1);
		return pindices[0].gIntAsSize;
	}

	// FIXME not here?
	int[] getIndices(ITreeModel model) const
	{
		GtkTreeIter iter;
		if(!entity.assumeNotModified.gtk_tree_selection_get_selected(null, &iter))
			return null;

		auto gtkTreePath = model.entity.assumeNotModified.gtk_tree_model_get_path(&iter);
		scope(exit) gtkTreePath.gtk_tree_path_free();
		gint depth;
		gint* pindices = gtkTreePath.gtk_tree_path_get_indices_with_depth(&depth);
		return pindices[0 .. depth.gIntAsSize].dup;
	}
}


interface ICellLayout: IObjectG
{
	alias entity = gtkCellLayout;

	final @property inout(GtkCellLayout)* gtkCellLayout() inout
	{ return cast(inout(GtkCellLayout)*) gObject; }

	final void addAttribute(CellRenderer cell, in char[] attribute, int column)
	{ entity.gtk_cell_layout_add_attribute(cell.entity, attribute.tempCString().assumeStringNotModified, column); }
}

class TreeViewColumn: ObjectG, ICellLayout
{
	@property inout(GtkTreeViewColumn)* gtkTreeViewColumn() inout
	{ return cast(inout(GtkTreeViewColumn)*) gObject; }

	alias entity = gtkTreeViewColumn;

	this(GtkTreeViewColumn* gtkTreeViewColumn)
	{ super(cast(GObject*) gtkTreeViewColumn); }

	this(in char[] header, CellRenderer renderer)
	{
		this(cast(GtkTreeViewColumn*) gtkNew!gtk_tree_view_column_new_with_attributes
			 (header.tempCString().assumeStringNotModified, renderer.entity, null));
	}

	this(in char[] header, CellRenderer renderer, in char[] type, int column)
	{
		this(cast(GtkTreeViewColumn*) gtkNewVA!gtk_tree_view_column_new_with_attributes
			 (header.tempCString().assumeStringNotModified, renderer.entity,
				type.tempCString().assumeStringNotModified, column, null));
	}
}


abstract class CellRenderer: ObjectG
{
	@property inout(GtkCellRenderer)* gtkCellRenderer() inout
	{ return cast(inout(GtkCellRenderer)*) gObject; }

	alias entity = gtkCellRenderer;

	this(GtkCellRenderer* gtkCellRenderer)
	{ super(cast(GObject*) gtkCellRenderer); }
}


class CellRendererText: CellRenderer
{
	@property inout(GtkCellRendererText)* gtkCellRendererText() inout
	{ return cast(inout(GtkCellRendererText)*) gObject; }

	alias entity = gtkCellRendererText;

	this(GtkCellRendererText* gtkCellRendererText)
	{
		super(cast(GtkCellRenderer*) gtkCellRendererText);
		mixin initGTKSignals;
		initSignals();
	}

	this()
	{ this(cast(GtkCellRendererText*) gtkNew!gtk_cell_renderer_text_new()); }


	GTKSignal!(connectEdited, string, string) edited;

	final void connectEdited()
	{
		extern(C) void gtkCallBack(GtkWidget*, gchar* path, gchar* newText, CellRendererText sender)
		{ sender.edited.callListeners(path.toString(), newText.toString()); }

		connectSignal("edited", cast(GCallback) &gtkCallBack);
	}
}


class CellRendererPixbuf: CellRenderer
{
	@property inout(GtkCellRendererPixbuf)* gtkCellRendererPixbuf() inout
	{ return cast(inout(GtkCellRendererPixbuf)*) gObject; }

	alias entity = gtkCellRendererPixbuf;

	this(GtkCellRendererPixbuf* gtkCellRendererPixbuf)
	{ super(cast(GtkCellRenderer*) gtkCellRendererPixbuf); }

	this()
	{ this(cast(GtkCellRendererPixbuf*) gtkNew!gtk_cell_renderer_pixbuf_new()); }
}


class CellRendererToggle: CellRenderer
{
	@property inout(GtkCellRendererToggle)* gtkCellRendererToggle() inout
	{ return cast(inout(GtkCellRendererToggle)*) gObject; }

	alias entity = gtkCellRendererToggle;

	this(GtkCellRendererToggle* gtkCellRendererToggle)
	{
		super(cast(GtkCellRenderer*) gtkCellRendererToggle);
		mixin initGTKSignals;
		initSignals();
	}

	this()
	{ this(cast(GtkCellRendererToggle*) gtkNew!gtk_cell_renderer_toggle_new()); }


	GTKSignal!(connectToggled, string) toggled;

	final void connectToggled()
	{
		extern(C) void gtkCallBack(GtkCellRendererToggle*, gchar* path, CellRendererToggle sender)
		{ sender.toggled.callListeners(path.toString()); }

		connectSignal("toggled", cast(GCallback) &gtkCallBack);
	}
}


class TreeView: Container
{
	@property inout(GtkTreeView)* gtkTreeView() inout
	{ return cast(inout(GtkTreeView)*) gObject; }

	alias entity = gtkTreeView;

	this(GtkTreeView* gtkTreeView)
	{
		super(cast(GtkContainer*) gtkTreeView);
		mixin initGTKSignals;
		initSignals();
	}

	this()
	{ this(cast(GtkTreeView*) gtkNew!gtk_tree_view_new()); }

	this(ITreeModel model)
	{ this(cast(GtkTreeView*) gtkNew!gtk_tree_view_new_with_model(model.entity)); }


	@property inout(ITreeModel) model() inout
	{
		return cast(inout) ObjectG.fromGObject!(ITreeModel, NativeTreeModel)(entity.assumeNotModified.gtk_tree_view_get_model());
	}

	@property inout(TreeSelection) selection() inout
	{
		return cast(inout) ObjectG.fromGObject!TreeSelection(entity.assumeNotModified.gtk_tree_view_get_selection());
	}

	@property void headersVisible(bool val)
	{ entity.gtk_tree_view_set_headers_visible(val); }


	void expandAll()
	{ entity.gtk_tree_view_expand_all(); }

	void expandRow(TreeModelRow row, bool openAll)
	{
		auto gtkTreePath = row.model.entity.gtk_tree_model_get_path(row.iter);
		scope(exit) gtkTreePath.gtk_tree_path_free();
		entity.gtk_tree_view_expand_row(gtkTreePath, openAll);
	}

	void scrollToCell(TreeModelRow row, TreeViewColumn column, bool useAlign, float rowAlign, float colAlign)
	{
		auto gtkTreePath = row.model.entity.gtk_tree_model_get_path(row.iter);
		scope(exit) gtkTreePath.gtk_tree_path_free();
		entity.gtk_tree_view_scroll_to_cell(gtkTreePath, column ? column.entity : null, useAlign, rowAlign, colAlign);
	}


	CellRendererText appendTextColumn(in char[] title, size_t id, bool useMarkup = false)
	{
		auto renderer = new CellRendererText();
		auto column = gtk_tree_view_column_new_with_attributes(
			title.tempCString().assumeStringNotModified,
			renderer.gtkCellRenderer,
			useMarkup ? "markup".assumeStringNotModified : "text".assumeStringNotModified,
			id.sizeAsGInt,
			null);
		entity.gtk_tree_view_append_column(column);
		return renderer;
	}

	void appendColumn(TreeViewColumn column)
	{ entity.gtk_tree_view_append_column(column.entity); }


	GTKSignal!(connectRowActivated, GtkTreePath*, TreeViewColumn) rowActivated;

	final void connectRowActivated()
	{
		extern(C) void gtkCallBack(GtkTreeView*, GtkTreePath* gtkTreePath, GtkTreeViewColumn* column, TreeView sender)
		{ sender.rowActivated.callListeners(gtkTreePath, ObjectG.fromGObject!TreeViewColumn(column)); }

		connectSignal("row-activated", cast(GCallback) &gtkCallBack);
	}
}


class Expander: Bin
{
	@property inout(GtkExpander)* gtkExpander() inout
	{ return cast(inout(GtkExpander)*) gObject; }

	alias entity = gtkExpander;

	this(GtkExpander* gtkExpander)
	{
		super(cast(GtkBin*) gtkExpander);
		mixin initGTKSignals;
		initSignals();
	}

	this(in char[] label = null, bool mnemonic = true)
	{
		// FIXME ? downcast to correct type in fromGObject?
		/+
		if(mnemonic)
			this(cast(GtkExpander*) gtkNew!gtk_expander_new_with_mnemonic(label.tempCString().assumeStringNotModified));
		else
			this(cast(GtkExpander*) gtkNew!gtk_expander_new(label.tempCString().assumeStringNotModified));
		+/
		if(mnemonic)
			this(cast(GtkExpander*) gtkNew!gtk_expander_new_with_mnemonic(null));
		else
			this(cast(GtkExpander*) gtkNew!gtk_expander_new(null));
		if(label)
			this.label = label;
	}


	@property void label(in char[] label)
	{
		if(label)
		{
			auto ll = new Label(label); // FIXME
			ll.show();
			labelWidget = ll;
		}
		else
		{
			entity.gtk_expander_set_label(null);
		}
	}


	@property bool useMarkup() const
	{ return !!entity.assumeNotModified.gtk_expander_get_use_markup(); }

	@property void useMarkup(bool val)
	{ entity.gtk_expander_set_use_markup(val); }


	@property bool expanded() const
	{ return !!entity.assumeNotModified.gtk_expander_get_expanded(); }

	@property void expanded(bool val)
	{ entity.gtk_expander_set_expanded(val); }


	@property inout(Widget) labelWidget() inout
	{
		return cast(inout) ObjectG.fromGObject!Widget(entity.assumeNotModified.gtk_expander_get_label_widget());
	}

	@property void labelWidget(Widget widget)
	{
		entity.gtk_expander_set_label_widget(widget.entity);
	}


	GTKSignal!connectActivate activate;

	final void connectActivate()
	{
		extern(C) void gtkCallBack(GtkExpander*, Expander sender)
		{ sender.activate.callListeners(); }

		connectSignal("activate", cast(GCallback) &gtkCallBack);
	}


	GTKSignal!connectAfterActivate afterActivate;

	final void connectAfterActivate()
	{
		extern(C) void gtkCallBack(GtkExpander*, Expander sender)
		{ sender.afterActivate.callListeners(); }

		connectSignal("activate", cast(GCallback) &gtkCallBack, ConnectFlags.AFTER);
	}
}


class ComboBox: Bin
{
	@property inout(GtkComboBox)* gtkComboBox() inout
	{ return cast(inout(GtkComboBox)*) gObject; }

	alias entity = gtkComboBox;

	this(GtkComboBox* gtkComboBox)
	{ super(cast(GtkBin*) gtkComboBox); }

	this(bool entry = false)
	{
		if(entry)
			this(cast(GtkComboBox*) gtkNew!gtk_combo_box_new_with_entry());
		else
			this(cast(GtkComboBox*) gtkNew!gtk_combo_box_new());
	}

	this(ITreeModel model, bool entry = false)
	{
		if(entry)
			this(cast(GtkComboBox*) gtkNew!gtk_combo_box_new_with_model_and_entry(model.entity));
		else
			this(cast(GtkComboBox*) gtkNew!gtk_combo_box_new_with_model(model.entity));
	}


	@property inout(ITreeModel) model() inout
	{
		return cast(inout) ObjectG.fromGObject!(ITreeModel, NativeTreeModel)(entity.assumeNotModified.gtk_combo_box_get_model());
	}


	@property size_t active() const
	{ return entity.assumeNotModified.gtk_combo_box_get_active().gIntAsSize; }

	@property void active(size_t val)
	{ entity.gtk_combo_box_set_active(val.sizeAsGInt); }


	void appendTextRenderer(size_t id, bool expand = true, bool ellipsizeEnd = true)
	{
		auto renderer = gtkNew!gtk_cell_renderer_text_new();
		if(ellipsizeEnd)
			g_object_set_property(cast(GObject*) renderer, "ellipsize".assumeStringNotModified, ValueG(cast(int) PangoEllipsizeMode.END).gValue);
		auto gtkCellLayout = cast(GtkCellLayout*) entity;
		gtkCellLayout.gtk_cell_layout_pack_start(renderer, expand);
		gtkCellLayout.gtk_cell_layout_add_attribute(renderer, "text".assumeStringNotModified, id.sizeAsGInt);
	}
}


class ComboBoxText: ComboBox
{
	@property inout(GtkComboBoxText)* gtkComboBoxText() inout
	{ return cast(inout(GtkComboBoxText)*) gObject; }

	alias entity = gtkComboBoxText;

	this(GtkComboBoxText* gtkComboBoxText)
	{ super(cast(GtkComboBox*) gtkComboBoxText); }

	this(bool entry = false)
	{
		if(entry)
			this(cast(GtkComboBoxText*) gtkNew!gtk_combo_box_text_new_with_entry());
		else
			this(cast(GtkComboBoxText*) gtkNew!gtk_combo_box_text_new ());
	}
}


class TextTagTable: ObjectG
{
	@property inout(GtkTextTagTable)* gtkTextTagTable() inout
	{ return cast(inout(GtkTextTagTable)*) gObject; }

	alias entity = gtkTextTagTable;

	this(GtkTextTagTable* gtkTextTagTable)
	{ super(cast(GObject*) gtkTextTagTable); }
}

class TextBuffer: ObjectG
{
	@property inout(GtkTextBuffer)* gtkTextBuffer() inout
	{ return cast(inout(GtkTextBuffer)*) gObject; }

	alias entity = gtkTextBuffer;

	this(GtkTextBuffer* gtkTextBuffer)
	{ super(cast(GObject*) gtkTextBuffer); }

	this(TextTagTable table = null)
	{
		this(gtkNew!gtk_text_buffer_new(table ? table.entity : null));
	}


	@property void text(in char[] val)
	{ entity.gtk_text_buffer_set_text(val.tempCString().assumeStringNotModified, val.length); }
}


class TextView: Container, INativeTextOutputControl
{
	@property inout(GtkTextView)* gtkTextView() inout
	{ return cast(inout(GtkTextView)*) gObject; }

	alias entity = gtkTextView;

	this(GtkTextView* gtkTextView)
	{ super(cast(GtkContainer*) gtkTextView); }

	this()
	{
		this(cast(GtkTextView*) gtkNew!gtk_text_view_new());
	}

	this(TextBuffer buffer)
	{
		this(cast(GtkTextView*) gtkNew!gtk_text_view_new_with_buffer(buffer.entity));
	}


	@property bool editable() const
	{ return !!entity.assumeNotModified.gtk_text_view_get_editable(); }

	@property void editable(bool val)
	{ entity.gtk_text_view_set_editable(val); }


	// INativeTextOutputControl implementation

	override void setTaggedText(string text, in Tag[] tags)
	{
		// FIXME: not tags support yet
		entity.gtk_text_view_get_buffer()
			.gtk_text_buffer_set_text(text.tempCString().assumeStringNotModified, text.length);
	}
}

class Notebook: Container
{
	@property inout(GtkNotebook)* gtkNotebook() inout
	{ return cast(inout(GtkNotebook)*) gObject; }

	alias entity = gtkNotebook;

	this(GtkNotebook* gtkNotebook)
	{ super(cast(GtkContainer*) gtkNotebook); }

	this()
	{
		this(cast(GtkNotebook*) gtkNew!gtk_notebook_new());
	}


	@property int currentPage() const
	{ return entity.assumeNotModified.gtk_notebook_get_current_page(); }

	@property void currentPage(int pageNum)
	{ entity.gtk_notebook_set_current_page(pageNum); }


	int appendPage(Widget child, Widget tabLabel)
	{
		return entity.gtk_notebook_append_page(child.entity, tabLabel ? tabLabel.entity : null);
	}

	int appendPage(Widget child, in char[] tabLabel)
	{
		return appendPage(child, new Label(tabLabel));
	}
}

class Spinner: Widget
{
	@property inout(GtkSpinner)* gtkSpinner() inout
	{ return cast(inout(GtkSpinner)*) gObject; }

	alias entity = gtkSpinner;

	this(GtkSpinner* gtkSpinner)
	{ super(cast(GtkWidget*) gtkSpinner); }

	this()
	{
		this(cast(GtkSpinner*) gtkNew!gtk_spinner_new());
	}


	void start()
	{
		entity.gtk_spinner_start();
	}

	void stop()
	{
		entity.gtk_spinner_stop();
	}
}



final class SimpleGTKPanelFactory(Base, NativeContext, NativePanels...): Base
{
	mixin nativePanelFactoryBase!(NativeContext, NativePanels);
}


__EOF__

unittest
{
	gtk_init(null, null);
	auto l = new Label();
	l.destroy();
	auto eb = new EntryBuffer();
	import std.stdio;
	eb.addOnValueChanged(s => writeln(s.value));
	eb.value = "aя!";
	eb.value = "b";
/*
	import gtk.Label: Label;
	import gobject.ObjectG: ObjectG;
	auto l = new ObjectG(cast(GObject*) gtk_label_new(null));
	l.destroy();*/
}
