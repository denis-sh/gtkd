﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtk.arrayeditor;


import metaui.controls.panel;

import gtkd.cutils;
import gtkd.gtk.utils;
import gtkd.gtk.base;
import gtkd.gtk.controls;
import gtkd.gtk.treemodel;
import gtkd.gobject.object_;
import gtkd.gobject.value;

import gtkc.gobject;
import gtkc.gtk;
import gtkc.gtktypes;


@trusted:

unittest
{
	import gtkc.gobject;
	g_type_init();
}



abstract class ArrayEditor(T, ElementEditor : Panel!E, E): Panel!T
{
	E[] _array;
	ElementEditor elementEditor;
	bool _arrrayUpdateing = false;
	Widget elementEditorRootWidget, noElementWidget;

	ListStore ls;
	TreeView treeView;
	TreeSelection treeSelection;
	Paned hpnd;
	Widget rootWidget;
	size_t index = -1;

	this(int position, ElementEditor elementEditor, Widget elementEditorRootWidget, Widget noElementWidget, in TypeG[] types...)
	{
		this.elementEditor = elementEditor;
		this.elementEditorRootWidget = elementEditorRootWidget;
		this.noElementWidget = noElementWidget;

		ls = new ListStore(types);
		treeView = new TreeView(ls);
		treeSelection = treeView.selection;

		treeSelection.changed ~= &onSelectionChanged;

		auto scr = ScrolledWindow.newAutomatic().with_(treeView);

		hpnd = Paned.newHorizontal(position);
		hpnd.pack1(scr, false);
		hpnd.pack2(noElementWidget);
		rootWidget = hpnd;
		rootWidget.showAll();

		super(new ContainerControl());
	}

protected:
	abstract inout(E)[] getArrayBuff(inout T obj) pure;
	abstract ValueG[] getValuesG(in E e);

	void updateArray()
	{
		_arrrayUpdateing = true;
		scope(exit) _arrrayUpdateing = false;
		ls.clear();
		_array = getArrayBuff(source).dup;

		size_t selectIdx = -1;
		foreach(i, const el; _array)
		{
			if(el is elementEditor.source)
				selectIdx = i;
			ls.appendValuesG(getValuesG(el));
		}

		if(selectIdx == -1)
			elementEditor.source = null;
		else
			treeSelection.selectIndex(ls, selectIdx);
	}

	override void connectSource()
	{
		updateArray();
	}

	override void disconnectSource()
	{
		treeSelection.unselectAll();
		_array = null;
	}

	void onSelectionChanged(TreeSelection)
	{
		if(_arrrayUpdateing)
			return;
		const idx = treeSelection.getSelectedIndex(ls);

		auto element = idx == -1 ? null : _array[idx];

		if(!element != !elementEditor.source)
		{
			hpnd.remove(element ? noElementWidget : elementEditorRootWidget);
			hpnd.add2(element ? elementEditorRootWidget : noElementWidget);
		}

		elementEditor.source = element;
	}
}

version(none)
unittest
{
	class E { string name; int n; }
	class C
	{
		E[] arrBuff;
	}
	class ElementEditor: Panel!E
	{
		Widget rootWidget;

		this()
		{
			super(new ContainerControl());
		}

protected:
		override void connectSource()
		{
		}

		override void disconnectSource()
		{
		}
	}
	class CArrEditor: ArrayEditor!(C, ElementEditor)
	{
		this()
		{
			import gtk.TreeViewColumn;
			import gtk.CellRendererText;

			auto elementEditor = new ElementEditor();
			super(elementEditor, elementEditor.rootWidget, TypeG.string, TypeG.int_);
			gtk_tree_view_append_column(gtkTreeView, gtk_tree_view_column_new_with_attributes(
				"Name".assumeStringNotModified,
				gtkNew!gtk_cell_renderer_text_new(),
				"text".assumeStringNotModified,
				0, null));
			gtk_tree_view_append_column(gtkTreeView, gtk_tree_view_column_new_with_attributes(
				"n".assumeStringNotModified,
				gtkNew!gtk_cell_renderer_text_new(),
				"text".assumeStringNotModified,
				1, null));
		}
	protected:
		override inout(E)[] getArrayBuff(inout C c) pure
		{ return c.arrBuff; }

		override ValueG[] getValuesG(in E e) @trusted
		{ return valuesGDynamicArray(e.name, e.n); }
	}
	auto ae = new CArrEditor();
}
