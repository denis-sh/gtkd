﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gtk.treemodel;


import std.range;

import unstd.memory.allocation;

import gtkd.cutils;
import gtkd.gtk.utils;
import gtkd.gtk.base;
import gtkd.gobject.object_;
import gtkd.gobject.value;

import gtkc.gtk;
import gtkc.gtktypes;


unittest
{
	import gtkc.gobject;
	g_type_init();
}


interface ITreeModel
{
	@property
	{
		alias entity = gtkTreeModel;

		inout(GtkTreeModel)* gtkTreeModel() inout;

		final inout(GtkTreeModel)* gtkStruct() inout
		{ return gtkTreeModel; }

		final TreeModelRows rows()
		{ return TreeModelRows(this); }

		final size_t columns() const
		{
			return gtk_tree_model_get_n_columns(gtkStruct.assumeNotModified).gIntAsSize;
		}
	}
}

class NativeTreeModel: ObjectG, ITreeModel
{
	@property inout(GtkTreeModel)* gtkTreeModel() inout
	{ return cast(inout(GtkTreeModel)*) gObject; }

	alias entity = gtkTreeModel;

	this(GtkTreeModel* gtkTreeModel)
	{ super(cast(GObject*) gtkTreeModel); }
}


interface IStore: ITreeModel
{
	void gtkAppend(ref GtkTreeIter newIterData);
	void gtkInsert(ref GtkTreeIter newIterData, gint position);
	void gtkInsertBefore(ref GtkTreeIter newIterData, GtkTreeIter* sibling);
	void gtkInsertAfter(ref GtkTreeIter newIterData, GtkTreeIter* sibling);
	void gtkSetValues(GtkTreeIter* iter, gint* columns, GValue* gValues, gint count);
	void gtkSetValue(GtkTreeIter* iter, gint column, GValue* gValue);
	bool gtkRemove(ref GtkTreeIter iter);
	void clear();

	final TreeModelRow append()
	{
		GtkTreeIter newIterData;
		gtkAppend(newIterData);
		return TreeModelRow(this, newIterData);
	}

	final TreeModelRow insert(gint position)
	{
		GtkTreeIter newIterData;
		gtkInsert(newIterData, position);
		return TreeModelRow(this, newIterData);
	}

	final TreeModelRow insertBefore(TreeModelRow sibling)
	{
		GtkTreeIter newIterData;
		gtkInsertBefore(newIterData, sibling.iter);
		return TreeModelRow(this, newIterData);
	}

	final TreeModelRow insertAfter(TreeModelRow sibling)
	{
		GtkTreeIter newIterData;
		gtkInsertAfter(newIterData, sibling.iter);
		return TreeModelRow(this, newIterData);
	}

	final TreeModelRow appendFirstValuesG(ValueG[] gValues...)
	{
		auto row = append();
		setFirstValuesG(row, gValues);
		return row;
	}

	final TreeModelRow appendFirstValues(A...)(A values)
	{
		auto row = append();
		setFirstValues(row, values);
		return row;
	}

	final TreeModelRow appendAllValuesG(ValueG[] gValues...)
	{
		auto row = append();
		setAllValuesG(row, gValues);
		return row;
	}

	final TreeModelRow appendAllValues(A...)(A values)
	{
		auto row = append();
		setAllValues(row, values);
		return row;
	}

	final bool remove(ref TreeModelRow row)
	in { assert(row.model is this); }
	body
	{
		return gtkRemove(row._iterData);
	}

	final void setValue(T)(TreeModelRow row, int column, T value)
	in { assert(row.model is this && column >= 0 && column < columns); }
	body
	{
		ValueG valueG = value;
		gtkSetValue(row.iter, column, valueG.gValue);
	}

	// TODO: setSomeValuesG

	final void setSomeValues(A...)(TreeModelRow row, A values)
	in
	{
		assert(row.model is this && A.length / 2 <= columns);
		foreach(const i, const value; values) static if(i % 2 == 0)
			assert(value >= 0 && value < columns);
	}
	body
	{
		static assert(A.length % 2 == 0);
		enum count = A.length / 2;
		gint[count] columns;
		ValueG[count] valuesG;
		foreach(const i, const value; values)
			static if(i % 2 == 0)
				columns[i / 2] = value;
			else
				valuesG[i / 2] = ValueG(value);
		gtkSetValues(row.iter, columns.ptr, valuesG.valuesGAsGValues, count.sizeAsGInt);
	}

	final void setValuesGFrom(TreeModelRow row, size_t startColumn, ValueG[] gValues...)
	in { assert(row.model is this && startColumn + gValues.length <= columns); }
	body
	{
		const count = gValues.length;
		auto tmpBuff = tempAlloc!gint(count, false);
		gint[] columns = tmpBuff.arr;
		foreach(const gint i; 0 .. count.sizeAsGInt)
			columns[i] = startColumn + i;
		gtkSetValues(row.iter, columns.ptr, cast(GValue*) gValues.ptr, count.sizeAsGInt);
	}

	final void setValuesFrom(A...)(TreeModelRow row, size_t startColumn, A values)
	in { assert(row.model is this && startColumn + A.length <= columns); }
	body
	{
		enum count = A.length;
		gint[count] columns;
		foreach(const gint i; 0 .. count.sizeAsGInt)
			columns[i] = startColumn + i;
		/+
		ValueG[count] valuesG;
		foreach(const i, const value; values)
			valuesG[i] = ValueG(value);
		+/
		ValueG[count] valuesG = valuesGStaticArray(values);
		gtkSetValues(row.iter, columns.ptr, valuesG.valuesGAsGValues, count.sizeAsGInt);
	}

	final void setFirstValuesG(TreeModelRow row, ValueG[] gValues...)
	in { assert(row.model is this && gValues.length <= columns); }
	body
	{
		setValuesGFrom(row, 0, gValues);
	}

	final void setFirstValues(A...)(TreeModelRow row, A values)
	in { assert(row.model is this && A.length <= columns); }
	body
	{
		setValuesFrom(row, 0, values);
	}

	final void setAllValuesG(TreeModelRow row, ValueG[] gValues...)
	in { assert(row.model is this && gValues.length == columns); }
	body
	{
		setFirstValuesG(row, gValues);
	}

	final void setAllValues(A...)(TreeModelRow row, A values)
	in { assert(row.model is this && A.length == columns); }
	body
	{
		setFirstValues(row, values);
	}
}


final class ListStore: ObjectG, IStore
{
	private GtkListStore* _gtkStruct;

	this(in TypeG[] types...)
	{
		_gtkStruct = gtkNew!gtk_list_store_newv(types.length.sizeAsGInt, cast(GType*) types.ptr.assumeNotModified);
		super(cast(GObject*) _gtkStruct);
	}

	@property
	{
		inout(GtkListStore)* gtkListStore() inout
		{ return _gtkStruct; }

		alias gtkStruct = gtkListStore;

		override inout(GtkTreeModel)* gtkTreeModel() inout
		{ return cast(typeof(return)) _gtkStruct; }
	}

	override void gtkAppend(ref GtkTreeIter newIterData)
	{ gtk_list_store_append(gtkStruct, &newIterData); }

	override void gtkInsert(ref GtkTreeIter newIterData, gint position)
	{ gtk_list_store_insert(gtkStruct, &newIterData, position); }

	override void gtkInsertBefore(ref GtkTreeIter newIterData, GtkTreeIter* sibling)
	{ gtk_list_store_insert_before(gtkStruct, &newIterData, sibling); }

	override void gtkInsertAfter(ref GtkTreeIter newIterData, GtkTreeIter* sibling)
	{ gtk_list_store_insert_after(gtkStruct, &newIterData, sibling); }

	override void gtkSetValues(GtkTreeIter* iter, gint* columns, GValue* gValues, gint count)
	{ gtk_list_store_set_valuesv(gtkStruct, iter, columns, gValues, count); }

	override void gtkSetValue(GtkTreeIter* iter, gint column, GValue* gValue)
	{ gtk_list_store_set_value(gtkStruct, iter, column, gValue); }

	override bool gtkRemove(ref GtkTreeIter iterData)
	{ return !!gtk_list_store_remove(gtkStruct, &iterData); }

	override void clear()
	{ gtk_list_store_clear(gtkStruct); }
}


final class TreeStore: ObjectG, IStore
{
	private GtkTreeStore* _gtkStruct;

	this(in TypeG[] types...)
	{
		_gtkStruct = gtkNew!gtk_tree_store_newv(types.length.sizeAsGInt, cast(GType*) types.ptr.assumeNotModified);
		super(cast(GObject*) _gtkStruct);
	}

	@property
	{
		inout(GtkTreeStore)* gtkTreeStore() inout
		{ return _gtkStruct; }

		alias gtkStruct = gtkTreeStore;

		override inout(GtkTreeModel)* gtkTreeModel() inout
		{ return cast(typeof(return)) _gtkStruct; }
	}

	override void gtkAppend(ref GtkTreeIter newIterData)
	{ gtk_tree_store_append(gtkStruct, &newIterData, null); }

	override void gtkInsert(ref GtkTreeIter newIterData, gint position)
	{ gtk_tree_store_insert(gtkStruct, &newIterData, null, position); }

	override void gtkInsertBefore(ref GtkTreeIter newIterData, GtkTreeIter* sibling)
	{ gtk_tree_store_insert_before(gtkStruct, &newIterData, null, sibling); }

	override void gtkInsertAfter(ref GtkTreeIter newIterData, GtkTreeIter* sibling)
	{ gtk_tree_store_insert_after(gtkStruct, &newIterData, null, sibling); }

	override void gtkSetValues(GtkTreeIter* iter, gint* columns, GValue* gValues, gint count)
	{ gtk_tree_store_set_valuesv(gtkStruct, iter, columns, gValues, count); }

	override void gtkSetValue(GtkTreeIter* iter, gint column, GValue* gValue)
	{ gtk_tree_store_set_value(gtkStruct, iter, column, gValue); }

	override bool gtkRemove(ref GtkTreeIter iterData)
	{ return !!gtk_tree_store_remove(gtkStruct, &iterData); }

	override void clear()
	{ gtk_tree_store_clear(gtkStruct); }

	TreeModelRow appendChild(in TreeModelRow parent)
	in { assert(parent.model is this); }
	body
	{
		GtkTreeIter newIterData;
		gtk_tree_store_append(_gtkStruct, &newIterData, parent.iter.assumeNotModified);
		return TreeModelRow(this, newIterData);
	}

	final TreeModelRow appendChildFirstValuesG(in TreeModelRow parent, ValueG[] gValues...)
	{
		auto row = appendChild(parent);
		setFirstValuesG(row, gValues);
		return row;
	}

	final TreeModelRow appendChildFirstValues(A...)(in TreeModelRow parent, A values)
	{
		auto row = appendChild(parent);
		setFirstValues(row, values);
		return row;
	}

	final TreeModelRow appendChildAllValuesG(in TreeModelRow parent, ValueG[] gValues...)
	{
		auto row = appendChild(parent);
		setAllValuesG(row, gValues);
		return row;
	}

	final TreeModelRow appendChildAllValues(A...)(in TreeModelRow parent, A values)
	{
		auto row = appendChild(parent);
		setAllValues(row, values);
		return row;
	}
}


struct TreeModelRows
{
	private
	{
		ITreeModel _model;
		bool _root;
		GtkTreeIter _parentIterData;
	}


	@disable this();

	private this(ITreeModel model, GtkTreeIter* parentIter = null)
	{
		_model = model;
		_root = !parentIter;
		if(!isRoot)
			_parentIterData = *parentIter;
	}


	@property
	{
		inout(ITreeModel) model() inout
		{ return _model; }

		bool isRoot() const
		{ return _root; }

		inout(GtkTreeIter)* parentIter() inout
		{ return isRoot ? null : &_parentIterData; }

		bool empty() const
		{ return isRoot ? !length : !gtk_tree_model_iter_has_child(model.gtkStruct.assumeNotModified, parentIter.assumeNotModified); }

		size_t length() const
		{ return gtk_tree_model_iter_n_children(model.gtkStruct.assumeNotModified, parentIter.assumeNotModified); }

		alias opDollar = length;

		TreeModelRow first()
		in { assert(!empty); }
		body
		{
			GtkTreeIter firstIterData;
			bool failed = !gtk_tree_model_iter_children(model.gtkStruct, &firstIterData, parentIter);
			assert(!failed);
			return TreeModelRow(model, firstIterData);
		}

		TreeModelRowsForwardRange forwardRange()
		{ return TreeModelRowsForwardRange(model, parentIter); }
	}


	TreeModelRow opIndex(size_t index)
	{
		GtkTreeIter childIterData;
		// FIXME add toNonNegativeGInt?
		bool failed = !gtk_tree_model_iter_nth_child(model.gtkStruct, &childIterData, parentIter, index);
		assert(!failed);
		return TreeModelRow(model, childIterData);
	}
}


struct TreeModelRow
{
	private
	{
		ITreeModel _model;
		GtkTreeIter _iterData;
	}

	@disable this();

	private this(ITreeModel model, GtkTreeIter iterData)
	{
		_model = model;
		_iterData = iterData;
	}

	@property
	{
		inout(ITreeModel) model() inout
		{ return _model; }

		inout(GtkTreeIter)* iter() inout
		{ return &_iterData; }

		bool hasParent() const
		{
			GtkTreeIter parentData;
			return !!gtk_tree_model_iter_parent(model.gtkStruct.assumeNotModified, &parentData, iter.assumeNotModified);
		}

		size_t columns() const
		{ return model.columns; }

		alias opDollar = columns;

		TreeModelRow parent()
		in { assert(hasParent); }
		body
		{
			GtkTreeIter parentIterData;
			bool failed = !gtk_tree_model_iter_parent(model.gtkStruct, &parentIterData, iter);
			assert(!failed);
			return TreeModelRow(model, parentIterData);
		}

		TreeModelRows children()
		{ return TreeModelRows(model, iter); }
	}

	// FIXME: invalidates iter
	bool tryMoveForward()
	{ return !!gtk_tree_model_iter_next(model.gtkStruct, iter); }

	bool tryMoveBack()
	{ return !!gtk_tree_model_iter_previous(model.gtkStruct, iter); }

	void opUnary(string op : "++")()
	{
		bool failed = !tryMoveForward();
		assert(!failed);
	}

	void opUnary(string op : "--")()
	{
		bool failed = !tryMoveBack();
		assert(!failed);
	}

	final ValueG getValueG(size_t column)
	in { assert(column < columns); }
	body
	{
		ValueG valueG;
		gtk_tree_model_get_value(model.gtkStruct, iter, column.sizeAsGInt, valueG.gValue);
		return valueG;
	}

	alias opIndex = getValueG;

	final T getValue(T)(size_t column)
	{
		return getValueG(column).as!T;
	}
}


struct TreeModelRowsForwardRange
{
	private
	{
		ITreeModel _model;
		GtkTreeIter _frontIterData;
		bool _empty;
	}

	@disable this();

	this(ITreeModel model, GtkTreeIter frontIterData)
	{
		_model = model;
		_frontIterData = frontIterData;
		_empty = false;
	}

	this(ITreeModel model, GtkTreeIter* parentIter = null)
	{
		_model = model;
		_empty = !gtk_tree_model_iter_children(model.gtkStruct, &_frontIterData, parentIter);
	}

	@property
	{
		inout(ITreeModel) model() inout
		{ return _model; }

		bool empty() const
		{ return _empty; }

		auto save()
		{ return this; }

		TreeModelRow front()
		in { assert(!_empty); }
		body
		{ return TreeModelRow(model, _frontIterData); }

		inout(GtkTreeIter)* frontIter() inout
		in { assert(!_empty); }
		body
		{ return &_frontIterData; }
	}

	void popFront()
	in { assert(!_empty); }
	body
	{ _empty = !gtk_tree_model_iter_next(model.gtkStruct, frontIter); }
}


unittest
{
	const types = [TypeG.int_, TypeG.char_, TypeG.string];
	foreach(store; [cast(IStore) new ListStore(types), cast(IStore) new TreeStore(types)])
	{
		scope(exit) destroy(store);
		with(store)
		{
			setAllValuesG(append(), ValueG(1), ValueG('a'), ValueG("x"));
			appendAllValues(2, 'b', "y");
		}
		with(store.rows[0])
			assert(getValue!int(0) == 1 && getValue!char(1) == 'a' && getValue!string(2) == "x");
		// dmd @@@BUG????@@@ workaround
		// auto row = store.rows[$ - 1];
		auto row = store.rows[store.rows.length - 1];
		assert(row[0].as!int == 2 && row[1].as!char == 'b' && row[$ - 1].as!string == "y");
	}
}


unittest
{
	TreeStore treeStore = new TreeStore(TypeG.int_);
	scope(exit) destroy(treeStore);

	TreeModelRows rows = treeStore.rows;
	with(rows)
	{
		assert(model is treeStore);
		assert(isRoot && !parentIter);
		assert(empty && !length);
	}
	with(rows.forwardRange)
		assert(model is treeStore && empty);

	static assert(isForwardRange!TreeModelRowsForwardRange);

	foreach(i; 0 .. 3)
	{
		const newRaw = treeStore.append();
		with(rows)
		{
			assert(!empty && length == i + 1);
			assert(forwardRange.walkLength() == i + 1);
		}
		with(rows[i])
		{
			assert(model is treeStore);
			assert(!hasParent);
			assert(children.empty);
			assert(*iter == *newRaw.iter);

			foreach(j; 0 .. 3)
			{
				const newSubRaw = treeStore.appendChild(newRaw);
				assert(!children.empty && children.length == j + 1);

				with(children[j])
				{
					assert(model is treeStore);
					assert(hasParent && *parent.iter == *newRaw.iter);
					assert(children.empty);
					assert(*iter == *newSubRaw.iter);
				}
			}
		}
	}
}
