﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gobject.object_;


import core.memory;

import unstd.c.string;

import gtkd.cutils;

import gtkc.gobject;
import gtkc.gobjecttypes;

import gtkd.gobject.value;


interface IObjectG
{
	alias entity = gObject;

	@property inout(GObject)* gObject() inout;
}

class ObjectG: IObjectG
{
	private GObject* _gObject;
	private bool _isGcRoot = false;

	@property inout(GObject)* gObject() inout
	{ return _gObject; }

	@property bool gObjectAlive() const
	{ return !!_gObject; }

	this(GObject* gObject)
	in
	{
		assert(gObject);
		assert(!g_object_get_data(gObject, dataKey.assumeStringNotModified), "D object is already created for this ObjectG.");
	}
	body
	{
		_gObject = gObject;

		extern(C) void destroyNotify(void* data)
		{
			auto obj = cast(ObjectG) data;
			assert(obj._gObject);
			assert(!obj._isGcRoot);
			obj._gObject = null;
		}

		extern(C) void toggleNotify(void* data, GObject*, int isLastRef)
		{
			auto obj = cast(ObjectG) data;
			assert(obj._isGcRoot == !!isLastRef);
			if (isLastRef)
				GC.removeRoot(cast(void*) obj), obj._isGcRoot = false;
			else
				GC.addRoot(cast(void*) obj), obj._isGcRoot = true;
		}

		g_object_set_data_full(_gObject, dataKey.assumeStringNotModified, cast(void*) this, &destroyNotify);
		g_object_add_toggle_ref(_gObject, &toggleNotify, cast(void*) this);

		assert(_gObject.refCount > 1);
		assert(!_isGcRoot);
		GC.addRoot(cast(void*) this), _isGcRoot = true;

		if(g_object_is_floating(_gObject))
		{
			g_object_ref_sink(_gObject);
			g_object_unref(_gObject);
		}
	}

	~this()
	{
		// FIXME GtkD usage
		import gtkc.paths;
		import gtkc.Loader;
		if(!Linker.isLoaded(LIBRARY.GOBJECT))
			return; // FIXME on final collect only
		if(gObjectAlive)
			g_object_unref(_gObject);
	}

	protected void connectSignal(immutable(char)* detailedSignal, GCallback cHandler, GConnectFlags connectFlags = cast(GConnectFlags) 0)
	{
		gObject.g_signal_connect_data(detailedSignal.assumeNotModified, cHandler, cast(void*) this, null, connectFlags);
	}
	

	void setPropertyG(string propertyName, ValueG valueG) // FIXME note: not in and even not const
	{
		g_object_set_property(gObject, propertyName.tempCString().assumeStringNotModified, valueG.gValue.assumeNotModified);
	}


	void setProperty(T)(string propertyName, T value)
	{
		setPropertyG(propertyName, ValueG(value));
	}


	static T fromGObject(T, U)(U gObject) if(is(T : ObjectG))
	{
		if(!gObject)
			return null;
		if(auto p = g_object_get_data(cast(GObject*) gObject, dataKey.assumeStringNotModified))
			return cast(T) p;
		else
			return new T(gObject);
	}

	// TODO check
	static I fromGObject(I, T, U)(U gObject) if(is(I == interface) && is(T : ObjectG) && is(T : I))
	{
		if(!gObject)
			return null;
		if(auto p = g_object_get_data(cast(GObject*) gObject, dataKey.assumeStringNotModified))
			return cast(I) cast(ObjectG) p;
		else
			return new T(gObject);
	}

	enum dataKey = "gtkd.gobject.object_.ObjectG";
}
