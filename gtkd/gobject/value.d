﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.gobject.value;


import core.stdc.config;

import std.traits;

import unstd.c.string;

import gtkd.cutils;

import gtkc.gobject;
import gtkc.gobjecttypes;


// GType
enum TypeG : size_t
{
	invalid = 0<<2,
	none = 1<<2,
//	interface_ = 2<<2,
//	c_char = 3<<2,
	char_ = 4<<2, // C's uchar
	bool_ = 5<<2,
	int_ = 6<<2,
	uint_ = 7<<2,
	c_long = 8<<2,
	c_ulong = 9<<2,
	long_ = 10<<2,
	ulong_ = 11<<2,
//	enum_ = 12<<2,
//	flags = 13<<2,
	float_ = 14<<2,
	double_ = 15<<2,
	string = 16<<2,
//	pointer = 17<<2,
//	boxed = 18<<2,
//	param = 19<<2,
//	object = 20<<2,
//	variant = 21<<2,

}

struct ValueG
{
	import std.exception;//FIXME

	GValue _gValueData;
	static assert(this.sizeof == GValue.sizeof);

	this(T)(in T t) @trusted
	{
		/* Unused:
	INVALID = 0<<2,
	NONE = 1<<2,
	INTERFACE = 2<<2,
	CHAR = 3<<2,
	LONG = 8<<2,
	ULONG = 9<<2,
	ENUM = 12<<2,
	FLAGS = 13<<2,
	POINTER = 17<<2,
	BOXED = 18<<2,
	PARAM = 19<<2,
	OBJECT = 20<<2,
	VARIANT = 21<<2,
		*/

		void init(in TypeG type)
		{
			//g_value_init(gValue, cast(GType) type);
			_gValueData.g_type = cast(GType) type;
			// Expect g_type_value_table_peek(type)->value_init_??? to zero-set data i.e. not needed here.
		}

		static if(is(T == bool))
		{
			init(TypeG.bool_);
			// g_value_set_boolean(gValue, t);
			_gValueData.data1.v_int = t;
		}
		else static if(is(T == long))
		{
			init(TypeG.long_);
			// g_value_set_int64(gValue, t);
			_gValueData.data1.v_int64 = t;
		}
		else static if(is(T == ulong))
		{
			init(TypeG.ulong_);
			// g_value_set_uint64(gValue, t);
			_gValueData.data1.v_uint64 = t;
		}
		else static if(is(T == int))
		{
			init(TypeG.int_);
			// g_value_set_int(gValue, t);
			_gValueData.data1.v_int = t;
		}
		else static if(is(T == uint))
		{
			init(TypeG.uint_);
			// g_value_set_uint(gValue, t);
			_gValueData.data1.v_uint = t;
		}
		else static if(is(T == char))
		{
			init(TypeG.char_); // FIXME Should we select schar/uchar???
			// g_value_set_uchar(gValue, t);
			_gValueData.data1.v_uint = t;
		}
		else static if(is(T == float))
		{
			init(TypeG.float_);
			// g_value_set_float(gValue, t);
			_gValueData.data1.v_float = t;
		}
		else static if(is(T == double))
		{
			init(TypeG.double_);
			// g_value_set_double(gValue, t);
			_gValueData.data1.v_double = t;
		}
		else static if(isSomeString!T)
		{
			init(TypeG.string);
			// g_value_take_string(gValue, t.toCStringGTryMalloc());
			_gValueData.data1.v_pointer = t.toCStringGTryMalloc();
			// Don't set G_VALUE_NOCOPY_CONTENTS to data1.v_uint
		}
		else
		{
			static assert(0, "Can't set ValueG with " ~ T.stringof);
		}
	}

	this(this)
	{
		if(!type)
			return;
		GValue newGValueData = { _gValueData.g_type };
		// Note: a copy will always own value. I.e.
		// G_VALUE_NOCOPY_CONTENTS will not be set in a copy.
		g_value_copy(gValue, &newGValueData);
		_gValueData = newGValueData;
	}

	~this()
	{
		// FIXME GtkD usage
		import gtkc.paths;
		import gtkc.Loader;
		if(!Linker.isLoaded(LIBRARY.GOBJECT))
			return; // FIXME on final collect only
		if(_gValueData.g_type)
			g_value_unset(gValue);
	}

	void reset()
	{ g_value_reset(gValue); }

	@property @trusted pure
	{
		/// Returns the _type of the value stored in this structure.
		TypeG type() const nothrow
		{ return cast(TypeG) _gValueData.g_type; } // TODO use g_value_get_gtype here?

		inout(GValue)* gValue() inout nothrow
		{ return &_gValueData; }

		T as(T)() inout
		{
			static if(is(T == bool))
				return asBool;
			else static if(is(T == long))
				return asLong;
			else static if(is(T == ulong))
				return asUlong;
			else static if(is(T == int))
				return asInt;
			else static if(is(T == uint))
				return asUint;
			else static if(is(T == char))
				return asChar;
			else static if(is(T == float))
				return asFloat;
			else static if(is(T == double))
				return asDouble;
			else static if(is(T == string))//FIXME returns not string
				return asCharArray.idup;
			else
				static assert(0, "Can't get ValueG as " ~ T.stringof);
		}

		bool asBool() const
		{
			enforce(type == TypeG.bool_);
			return !!_gValueData.data1.v_int;
		}

		long asLong() const
		{
			enforce(type == TypeG.long_);
			return _gValueData.data1.v_int64;
		}

		ulong asUlong() const
		{
			enforce(type == TypeG.ulong_);
			return _gValueData.data1.v_uint64;
		}

		c_long asCLong() const
		{
			enforce(type == TypeG.c_long);
			return _gValueData.data1.v_long;
		}

		c_ulong asCUlong() const
		{
			enforce(type == TypeG.c_ulong);
			return _gValueData.data1.v_ulong;
		}

		int asInt() const
		{
			enforce(type == TypeG.int_);
			return _gValueData.data1.v_int;
		}

		uint asUint() const
		{
			enforce(type == TypeG.uint_);
			return _gValueData.data1.v_uint;
		}

		char asChar() const
		{
			enforce(type == TypeG.char_); // FIXME Should other chars???
			return cast(char) _gValueData.data1.v_int;
		}

		float asFloat() const
		{
			enforce(type == TypeG.float_);
			return _gValueData.data1.v_float;
		}

		double asDouble() const
		{
			enforce(type == TypeG.double_);
			return _gValueData.data1.v_double;
		}

		inout(char)[] asCharArray() inout
		{
			enforce(type == TypeG.string);
			return (cast(inout char*) _gValueData.data1.v_pointer).asArray;
		}
	}

	bool opEquals(const ValueG rhs) const
	{
		return opEquals(rhs);
	}

	bool opEquals(const ref ValueG rhs) const nothrow
	{
		if(type != rhs.type)
			return false;
		const d = _gValueData.data1;
		const dRhs = rhs._gValueData.data1;
		final switch(type)
		{
			case TypeG.invalid:
			case TypeG.none:
				return false;
			case TypeG.bool_:
			case TypeG.char_:
			case TypeG.int_:
			case TypeG.uint_:
				return d.v_uint == dRhs.v_uint;
			case TypeG.c_long:
			case TypeG.c_ulong:
				return d.v_ulong == dRhs.v_ulong;
			case TypeG.long_:
			case TypeG.ulong_:
				return d.v_uint64 == dRhs.v_uint64;
			case TypeG.float_:
				return d.v_float == dRhs.v_float;
			case TypeG.double_:
				return d.v_double == dRhs.v_double;
			case TypeG.string:
				return !d.v_pointer == !dRhs.v_pointer
					&& (!d.v_pointer || equalCStrings(cast(const char*) d.v_pointer, cast(const char*) dRhs.v_pointer));
		}
	}

	string toString() const
	{
		return g_strdup_value_contents(gValue.assumeNotModified).toStringAndGFree();
	}
}

@property inout(GValue)* valuesGAsGValues(inout ValueG[] valuesG) pure nothrow
{ return cast(inout GValue*) valuesG.ptr; }

ValueG[A.length] valuesGStaticArray(A...)(A values)
{
	ValueG[A.length] valuesG;
	foreach(const i, const value; values)
		valuesG[i] = ValueG(value);
	return valuesG;
}

ValueG[] valuesGDynamicArray(A...)(A values)
{
	ValueG[] valuesG = new ValueG[A.length];
	foreach(const i, const value; values)
		valuesG[i] = ValueG(value);
	return valuesG;
}

version(none) unittest
{
	import gtkc.glib;

	g_type_init();
	GValue gValueData;
	GValue* gValue = &gValueData;

	g_mem_profile();
	char* gstr = "abc".toCStringGTryMalloc();
	g_mem_profile();
	g_free(gstr);
	g_mem_profile();
	g_free(gstr);
	g_free(gstr);
	g_value_init(gValue, GType.STRING);
	g_value_take_string(gValue, gstr);
}

unittest
{
	g_type_init();

	ValueG valueGStr = "x", valueGInt = 5;
	valueGStr = valueGInt;

	import unstd.generictuple;
	foreach(const val; expressionTuple!(
		true, false,
		-1L, 0L, 1L, long.max,
		0UL, 1UL, ulong.max,
		-1, 0, 1, int.max,
		0U, 1U, uint.max,
		'a', 'z', '\0',
		-1.1f, -1f, 0f, 1f, 1.1f, float.max,
		-1.1, -1.0, 0.0, 1.0, 1.1, double.max,
		cast(string) null, "", "abc"))
	{
		const ValueG valueG = val;
		assert(valueG.as!(typeof(val)) == val);
		assert(valueG == ValueG(val));
		const ValueG valueG2 = val;
		assert(valueG == valueG2);

		assert(valueG != ValueG.init);

		static if(is(typeof(val) == string))
			assert(valueG != ValueG(val ~ " "));
		else static if(is(typeof(val) == bool))
			assert(valueG != ValueG(!val));
		else static if(isIntegral!(typeof(val)) || is(typeof(val) == char))
			assert(valueG != ValueG(val + 1));
		else static if(isFloatingPoint!(typeof(val)))
			assert(valueG != ValueG(val ? val / 2 : val + 1));
		else
			static assert(0);
	}
}
