﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.glib.utils;


import std.algorithm;
import std.range;
import std.string;
static import std.uni;

import metaui.markup;
import gtkd.cutils;

import gtkc.glib: g_markup_escape_text;
import gtkc.glibtypes: gint, guint;


@trusted:

@property size_t gIntAsSize(gint gInt) @safe
//in { assert(gInt >= 0); } FIXME -1?
//body
{ return cast(size_t) gInt; }

@property gint sizeAsGInt(size_t size) @safe
//in { assert(size < cast(size_t) gint.max); } FIXME -1?
in { assert(cast(gint) size == size); }
body
{ return cast(gint) size; }

@property guint sizeAsGUInt(size_t size) @safe
//in { assert(size < cast(size_t) gint.max); } FIXME -1?
in { assert(cast(gint) size == size); }
body
{ return cast(guint) size; }

@property gint chars(in char[] str) @safe
{ return str.walkLength().sizeAsGInt; }


string gMarkupEscapeText(in char[] str)
{
	return g_markup_escape_text(str.ptr.assumeStringNotModified, str.length).toStringAndGFree();
}

string taggedTextToGMarkup(string text, string[string] tagsG, in Tag[] tags)
{
	string getXMLTag(string name) @safe
	{ return tagsG[name]; }

	return taggedTextToMarkup(text, tags,
		s => s.gMarkupEscapeText(),
		(tag) @trusted { return format("<%s>", getXMLTag(tag.name)); },
		(tag) @trusted { return format("</%s>", getXMLTag(tag.name).findSplitBefore!((a, string b) => std.uni.isWhite(a))("")[0]); });
}

unittest
{
	alias toGMarkup = taggedTextToGMarkup;
	assert(toGMarkup("A", [null: "b"], [Tag(null, 0, 1)]) == "<b>A</b>");
	assert(toGMarkup("<A>", [null: "b"], [Tag(null, 0, 3)]) == "<b>&lt;A&gt;</b>");
	assert(toGMarkup("ABC", [null: "b", "i": "i"], [Tag("i", 0, 1), Tag(null, 2, 3)]) == "<i>A</i>B<b>C</b>");
	assert(toGMarkup("ABC", [null: "u", "i": "i"], [Tag(null, 0, 2), Tag("i", 0, 1)]) == "<u><i>A</i>B</u>C");
	assert(toGMarkup("A", [null: "span color='#f00'"], [Tag(null, 0, 1)]) == "<span color='#f00'>A</span>");
}
