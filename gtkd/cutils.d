﻿/** GTKD project file.

Copyright: Denis Shelomovskij 2013

License: $(HTTP boost.org/LICENSE_1_0.txt, Boost License 1.0).

Authors: Denis Shelomovskij
*/
module gtkd.cutils;


import unstd.c.string;

import gtkc.glib: g_try_malloc, g_free;


@safe:

alias toCStringGTryMalloc = toCString!(g_try_malloc, char, char);
alias toStringAndGFree = moveToString!(g_free, char);

string buffToString(in char[] buff)
in { assert(buff); }
body
{ return buff.length ? buff.idup : ""; }

@property char* assumeStringNotModified(const(char)* s) @system
{ return cast(char*) s; }

@property T* assumeNotModified(T)(const(T)* t) @system
{ return cast(T*) t; }
